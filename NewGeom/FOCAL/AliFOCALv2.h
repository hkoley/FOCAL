#ifndef ALIFOCALV2_H
#define ALIFOCALV2_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */

/* $Id: AliFOCALv2.h 8621 2021-06-1 Norbert Novitzky $ */

////////////////////////////////////////////////
//  Manager and hits classes for set:FOCAL      //
////////////////////////////////////////////////

// This version of FOCAL assume the spagetti HCAL
// It also builds the FOCAL in different way from v0
// It builds the ECAL and HCAL seperately
// For the ECAL: it builds it tower by tower
// For the HCAL: it creates it fiber by fiber and tube by tube,
// then combines both assemblies of tubes and fibers

#include "AliFOCAL.h"
#include "AliFOCALGeometry.h"
#include "TGeoManager.h"

//___________________________________________

class AliFOCALGeomtory;

class AliFOCALv2 : public AliFOCAL
{

public:
  AliFOCALv2();
  AliFOCALv2(const char *name, const char *title);
  AliFOCALv2(const char *name, const char *title, const char *gfile);
  AliFOCALv2 &operator=(const AliFOCALv2 &ali);

  virtual ~AliFOCALv2();
  virtual void CreateGeometry();
  virtual void CreateECAL();
  virtual void CreateHCALSpaghetti();
  virtual void CreateHCALSandwich();
  virtual void CreateMaterials();
  virtual void Init();
  virtual Int_t IsVersion() const { return 2; }
  virtual void StepManager();

  void SetStoreFullShower(Bool_t flag) { fStoreFullShower = flag; }
  virtual void DrawModule() const;
  virtual void AddAlignableVolumes() const; // define sym.names for alignable volumes
  virtual void AddAlignableVolumesECAL() const;
  virtual void AddAlignableVolumesHCAL() const;
  virtual void FinishPrimary();

private:
  TObjArray *fGeometryObject;
  TString fGeometryFile;  // Name of geometry file
  Int_t fMedSens;         // Sensitive Medium (Si)
  Int_t fMedSensHCal;     // Sensitive Medium for HCal
  Int_t fStoreFullShower; // Flag to store full shower history (uses a lot of disk space an memery; not recommended)
  Int_t fCurrentTrack;    //! Current track in StepManager
  Int_t fCurrentMother;   //! Current morther track (track hitting FOCAL) in StepManager

  ClassDef(AliFOCALv2, 4) //Hits manager for set:FOCAL
};

#endif
