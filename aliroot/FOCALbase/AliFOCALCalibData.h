#ifndef ALIFOCALCALIBDATA_H
#define ALIFOCALCALIBDATA_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */


class TNamed;
class AliCDBEntry;
class AliFOCAL;

class AliFOCALCalibData: public TNamed
{
 public:
  AliFOCALCalibData();
  AliFOCALCalibData(const char* name);
  AliFOCALCalibData(const AliFOCALCalibData &pedestal);
  AliFOCALCalibData& operator= (const AliFOCALCalibData &pedestal);
  virtual ~AliFOCALCalibData();
  void  Reset();
  void  SetGainFactor(Int_t sec, Int_t seg, Int_t row, Int_t col,
		      Float_t gain);
  Float_t GetGainFactor(Int_t sec, Int_t seg, Int_t row, Int_t col) const;
  
 protected:

  enum
      {
	kSec    = 900,
	kSeg    = 3,  // Number of modules per plane
	kRow    = 7,  // Row
	kCol    = 2   // Column
      };

Float_t fGainFactor[kSec][kSeg][kRow][kCol];

ClassDef(AliFOCALCalibData,3) // CalibData class
};
#endif
