#ifndef ALIFOCALCELL_H
#define ALIFOCALCELL_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */
//-----------------------------------------------------//
//                                                     //
//                                                     //
//  Date   : July  30 2009                             //
//                                                     //
//  Utility class for FOCAL                            //
//                                                     //
//-----------------------------------------------------//
// Author - T. Gunji
//

#include "Rtypes.h"
#include "TObject.h"
class TClonesArray;

#include "AliLog.h"

class AliFOCALCell: public TObject
{

 public:
  AliFOCALCell();
  AliFOCALCell(Int_t it,  Int_t row, Int_t col, Int_t segment, 
		Int_t tower, Float_t x, Float_t y, Float_t z, Float_t e);
  AliFOCALCell(AliFOCALCell *fCell);
  AliFOCALCell (const AliFOCALCell &fCell);
  AliFOCALCell &operator=(const AliFOCALCell &fCell);
  
  // For comparing Cells
  Int_t Compare(const TObject * obj) const;
  bool IsSortable() const {return true;}

  virtual ~AliFOCALCell();
 
  virtual void SetCellID(int id){fCellid = id;}
  virtual void SetRow(int id){fRow = id;}
  virtual void SetCol(int id){fCol = id;}
  virtual void SetSegment(int id){fSegment = id;}
  virtual void SetTower(int id){fTower = id;}
  virtual void SetX(Float_t val){fX = val;}
  virtual void SetY(Float_t val){fY = val;}
  virtual void SetZ(Float_t val){fZ = val;}
  virtual void SetE(Float_t val){fE = val;}
  virtual void SetWeight(Float_t val){fWeight = val;}
  virtual void SetSeedEnergy(Float_t val){fSeedEnergy = val;}
  virtual void AddWeight(Float_t val){fWeight += val; if (fWeight < -1.) AliError(Form("Negative weight %f added %f at %f %f",fWeight,val,fX,fY)); }
  virtual void SetCellCenter(bool val){fCenter = val;}
  //  virtual void SetChildCell(int i, int val){fChildCells[i]= val ; fNumberOfChildCells++;}

  virtual Int_t   CellID() const {return fCellid;}
  virtual Int_t   Row() const {return fRow;}
  virtual Int_t   Col() const {return fCol;}
  virtual Int_t   Segment() const {return fSegment;}
  virtual Int_t   Tower() const { return fTower;}
  virtual Float_t X() const {return fX; }
  virtual Float_t Y() const {return fY; }
  virtual Float_t Z() const {return fZ; }
  virtual Float_t E() const {return fE; }
  virtual Float_t Weight() const {return fWeight;}
  virtual Float_t SeedEnergy() const {return fSeedEnergy;}
  virtual Bool_t  Center() const {return fCenter;}
  virtual Float_t LogWeightE(Float_t totalE, Float_t w0, bool mode);	// returns the energy-based weight for computing cluster properties, different functionality then Weight() above
  virtual Float_t LogWeight(Float_t totalE, Float_t w0);	// returns Terry's energy-based log weight
  virtual Float_t PowWeightE(Float_t totalE, Float_t w0, bool mode);	// returns the energy-based weight for computing cluster properties, different functionality then Weight() above
  virtual Float_t PowWeight(Float_t totalE, Float_t w0);	// returns an improvised energy-based power-law weight

 private:
  Int_t fCellid;  // pad id in the wafer (NxN pads)  
  Int_t fRow ;    // x location of the wafer in the brick
  Int_t fCol ;    // y location of the wafer in the brick 
  Int_t fSegment; // segments (0-2)
  Int_t fTower;   // tower, the cell is in
  Float_t fX;
  Float_t fY;
  Float_t fZ;
  Float_t fE;
  Float_t fWeight; // Total weight  
  Float_t fSeedEnergy;
  bool    fCenter;
  //  Int_t   fNumberOfChildTowers;
  //  Int_t   fChildTowers[100];

  
  ClassDef(AliFOCALCell,4) // Utility class for the detector set:FOCAL
};

#endif
