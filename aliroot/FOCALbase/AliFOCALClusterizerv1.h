#ifndef ALIFOCALClusterizerv1_H
#define ALIFOCALClusterizerv1_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
* See cxx source for full Copyright notice                               */
//-----------------------------------------------------//
//                                                     //
//                                                     //
//  Date   : July  16 2010                             //
//                                                     //
//  Utility class for FOCAL                            //
//                                                     //
//-----------------------------------------------------//
// Author - T. Gunji
//
// Base class for the clusterization algorithm 

#include "AliFOCALClusterizer.h"
#include "AliFOCALDigitizer.h"

class AliFOCALCluster;
class AliFOCALGeometry;
class AliFOCALPad;
class AliFOCALCell;
class AliFOCALhit;


class AliFOCALClusterizerv1 : public AliFOCALClusterizer
{

	public:
		AliFOCALClusterizerv1() ;        // default constructor 
		AliFOCALClusterizerv1(const AliFOCALClusterizerv1 &ali);
		AliFOCALClusterizerv1(AliFOCALClusterizerv1 *ali);
		AliFOCALClusterizerv1 &operator=(const AliFOCALClusterizerv1 &ali);
		virtual ~AliFOCALClusterizerv1() ; // destructor 
		
		virtual void AnalyzeEvent(TTree *tree, int type);
		virtual void Digits2Clusters(Option_t *option);     // Digits to cluster (Digits -> Cell -> Clusterizer)
		virtual void Hits2Clusters(Option_t *option);       // Hits to cluster (Hits -> Pad -> Cell -> Clusterizer)
		virtual void RunCluster(int Segments, TObjArray *cluster);
		
		virtual void SetClusteringEnergyThreshold(float cluth) { fClusteringEnergyThreshold = cluth; }
		virtual void SetLocalEnergyThreshold(float mine) { fLocalEnergyThreshold = mine; }
		virtual void SetChi2Limit(float min) { fCHI2Limit = min; }		
		virtual void SetDistance(float min) { fDistance = min; } // function to SET Distance
		virtual void SetSegLayer(bool var) { fSegLayer = var; }

		virtual void GetNumberOfClustersFound(int numb )const { numb = fNumberOfClusters; }
		virtual float GetDistance() { return fDistance; } // function to GET Distance
		virtual float GetClusteringEnergyThreshold() const { return fClusteringEnergyThreshold; }
		virtual float GetLocalEnergyThreshold() const { return fLocalEnergyThreshold; }
		
		virtual const char *Version() const { return "clustering of FoCAL v1"; }
		
		void InitGeometry(char *geometryfile);
		void InitGeometry();
			
	protected:
		virtual void MakeClusters();
		
	private:
		/// Functions
		void LoadHits2Pad(Option_t *option);
		void Pads2Cells(Option_t *option);   // Convert from Pad -> Cell info.
		void LoadDigits2Cell();    // Loading Digits and filling to Array of Pads
		void LoadHits2Cells();
		bool Neighboring_Cells(AliFOCALCell *twr1, AliFOCALCell *twr2);
		
		/// Variables
		AliFOCALGeometry *fGeom;
		float fCHI2Limit;
		int max_segment; /// variable with value of maximum segment "hitted" by particles in FoCal
		int fNumberOfClusters; // number of clusters found in EC section
		int fNumberOfClustersItr; // number of clusters found in EC section
		float fClusteringEnergyThreshold; // 
		float fLocalEnergyThreshold; // 
		float fDistance; // Variable to limit the distance to look clusters
		bool fSegLayer; // if fSegLayer == true it's saved the position of cluster of all segment, otherwise it's save 
					    // the cluster position per segment
		
	/// ROOT Stuff
	ClassDef(AliFOCALClusterizerv1,3);  // Clusterization algorithm class
} ;

#endif // AliFOCALLCLUSTERIZER_H
