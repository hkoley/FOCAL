/***************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/
//-----------------------------------------------------//
//                                                     //
//                                                     //
//  Date   : July 30   2009                            //
//                                                     //
//  FoCAL Z-Composition code for ALICE-FOCAL           //
//                                                     //
//-----------------------------------------------------//


#include "Riostream.h"
#include "TMath.h"
#include "TText.h"
#include "TLine.h"

#include <stdio.h>
#include <math.h>

#include "AliFOCALComposition.h"


ClassImp(AliFOCALComposition)

AliFOCALComposition::AliFOCALComposition():
  TObject(),
  fMaterial(0),
  fLayer(0),
  fStack(0),
  fId(0),
  fCenterX(0),
  fCenterY(0),
  fCenterZ(0),
  fSizeX(0),
  fSizeY(0),
  fSizeZ(0)
{
  //Default constructor
}

AliFOCALComposition::AliFOCALComposition(TString material, Int_t layer, Int_t stack, Int_t id, 
					 Float_t cx, Float_t cy, Float_t cz,
					 Float_t dx, Float_t dy, Float_t dz):
  TObject(),
  fMaterial(material),
  fLayer(layer),
  fStack(stack),
  fId(id),
  fCenterX(cx),
  fCenterY(cy),
  fCenterZ(cz),
  fSizeX(dx),
  fSizeY(dy),
  fSizeZ(dz)
{
  //Default constructor
}

AliFOCALComposition::AliFOCALComposition(AliFOCALComposition *fComposition):
  TObject(),
  fMaterial(0),
  fLayer(0),
  fStack(0),
  fId(0),
  fCenterX(0),
  fCenterY(0),
  fCenterZ(0),
  fSizeX(0),
  fSizeY(0),
  fSizeZ(0)
{
  
  *this = fComposition; 

}
AliFOCALComposition::AliFOCALComposition(const AliFOCALComposition &fComposition):
  TObject(),
  fMaterial(fComposition.fMaterial),
  fLayer(fComposition.fLayer),
  fStack(fComposition.fStack),
  fId(fComposition.fId),
  fCenterX(fComposition.fCenterX),
  fCenterY(fComposition.fCenterY),
  fCenterZ(fComposition.fCenterZ),
  fSizeX(fComposition.fSizeX),
  fSizeY(fComposition.fSizeY),
  fSizeZ(fComposition.fSizeZ)
{


}

AliFOCALComposition & AliFOCALComposition::operator=(const AliFOCALComposition &fComposition){
  if(this != &fComposition){
    fMaterial = fComposition.fMaterial;
    fLayer = fComposition.fLayer;
    fStack = fComposition.fStack;
    fId = fComposition.fId;
    fCenterX = fComposition.fCenterX ; 
    fCenterY = fComposition.fCenterY ; 
    fCenterZ = fComposition.fCenterZ ; 
    fSizeX = fComposition.fSizeX;
    fSizeY = fComposition.fSizeY;
    fSizeZ = fComposition.fSizeZ;
  }
  return *this;
}

AliFOCALComposition::~AliFOCALComposition()
{
  // Default destructor
}


