/***************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/
//-----------------------------------------------------//
//                                                     //
//  Source File : AliFOCALDigitizer.cxx, Version 00         //
//                                                     //
//  Date   : August 03 2009                           //
//  Written by Taku Gunji
//                                                     //
//-----------------------------------------------------//

#include <Riostream.h>
#include <TBRIK.h>
#include <TNode.h>
#include <TTree.h>
#include <TGeometry.h>
#include <TObjArray.h>
#include <TClonesArray.h>
#include <TFile.h>
#include <TNtuple.h>
#include <TParticle.h>
#include <TRandom.h>
#include <TMath.h>


#include "AliLog.h"
#include "AliRun.h"
#include "AliHit.h"
#include "AliDetector.h"
#include "AliRunLoader.h"
#include "AliLoader.h"
#include "AliConfig.h"
#include "AliMagF.h"
#include "AliDigitizer.h"
#include "AliHeader.h"
#include "AliCDBManager.h"
#include "AliCDBStorage.h"
#include "AliCDBEntry.h"
#include "AliMC.h"

#include "AliFOCAL.h"
#include "AliFOCALhit.h"
#include "AliFOCALDigitizer.h"
#include "AliFOCALsdigit.h"
#include "AliFOCALPedestal.h"
#include "AliFOCALCalibData.h"
#include "AliFOCALGeometry.h"
#include "AliFOCALdigit.h"


ClassImp(AliFOCALDigitizer)

using namespace std;

AliFOCALDigitizer::AliFOCALDigitizer() :
  fRunLoader(0),
  fFOCAL(0),
  fFOCALLoader(0),
  fGeom(0),
  fDoDigits(false),
  fDoSDigits(false),
  fLocalMode(false),
  fSDigits(0),
  fDigits(0),
  fSegmentMap(0),
  fSSegmentMap(0),
  fOutputFile(0),
  fTreeS(0),
  fTreeD(0),
  fNSDigit(0),
  fNDigit(0),
  fPixelNoiseProb(0.),
  fPadNoiseSigma(0)
{
  AliDebug(1,"AliFOCALDigitizer's default contructor called, initialization necessary.");
}

//____________________________________________________________________________
AliFOCALDigitizer::AliFOCALDigitizer(const char * pathToGalice, char * geometryFile, Option_t *option) :
  fRunLoader(0),
  fFOCAL(0),
  fFOCALLoader(0),
  fGeom(0),
  fDoDigits(false),
  fDoSDigits(false),
  fLocalMode(false),
  fSDigits(0),
  fDigits(0),
  fSegmentMap(0),
  fSSegmentMap(0),
  fOutputFile(0),
  fTreeS(0),
  fTreeD(0),
  fNSDigit(0),
  fNDigit(0),
  fPixelNoiseProb(0),
  fPadNoiseSigma(0)
{
  Initialize(pathToGalice, geometryFile, option);
}

//____________________________________________________________________________
AliFOCALDigitizer::AliFOCALDigitizer(const char * pathToGalice, char * geometryFile, const char * outputFile, Option_t *option) :
  fRunLoader(0),
  fFOCAL(0),
  fFOCALLoader(0),
  fGeom(0),
  fDoDigits(false),
  fDoSDigits(false),
  fLocalMode(false),
  fSDigits(0),
  fDigits(0),
  fSegmentMap(0),
  fSSegmentMap(0),
  fOutputFile(0),
  fTreeS(0),
  fTreeD(0),
  fNSDigit(0),
  fNDigit(0),
  fPixelNoiseProb(0),
  fPadNoiseSigma(0)
{
  InitializeLocal(pathToGalice, geometryFile, outputFile, option);
}

//____________________________________________________________________________
AliFOCALDigitizer::AliFOCALDigitizer(const AliFOCALDigitizer& digitizer):
  AliDigitizer(digitizer),
  fRunLoader(0),
  fFOCAL(0),
  fFOCALLoader(0),
  fGeom(0),
  fDoDigits(false),
  fDoSDigits(false),
  fLocalMode(false),
  fSDigits(0),
  fDigits(0),
  fSegmentMap(0),
  fSSegmentMap(0),
  fOutputFile(0),
  fTreeS(0),
  fTreeD(0),
  fNSDigit(0),
  fNDigit(0),
  fPixelNoiseProb(0.),
  fPadNoiseSigma(0)
{
  // copy constructor
  AliError("Copy constructor not allowed ");
}

//____________________________________________________________________________
AliFOCALDigitizer& AliFOCALDigitizer::operator=(const AliFOCALDigitizer& /*digitizer*/)
{
  // Assignment operator
  AliError("Assignement operator not allowed ");
  
  return *this;
}

////____________________________________________________________________________
AliFOCALDigitizer::AliFOCALDigitizer(AliDigitizationInput* manager):
  AliDigitizer(manager,"AliFOCALDigitizer","AliFOCALDigitizer"),
  fRunLoader(0),
  fFOCAL(0),
  fFOCALLoader(0),
  fGeom(0),
  fDoDigits(false),
  fDoSDigits(false),
  fLocalMode(false),
  fSDigits(0),
  fDigits(0),
  fSegmentMap(0),
  fSSegmentMap(0),
  fOutputFile(0),
  fTreeS(0),
  fTreeD(0),
  fNSDigit(0),
  fNDigit(0),
  fPixelNoiseProb(0.),
  fPadNoiseSigma(0.)
{
}

//____________________________________________________________________________
AliFOCALDigitizer::~AliFOCALDigitizer()
{
  // Default Destructor

  if (fSDigits) {
    fSDigits->Clear();
    delete fSDigits;
    fSDigits=0;
  }

  if (fDigits) {
    fDigits->Clear();
    delete fDigits;
    fDigits=0;
  }

  if (fGeom) {
    delete fGeom;
    fGeom = 0;
  }

  if (fSegmentMap) {
    delete [] fSegmentMap;
  }

  if (fSSegmentMap) {
    delete [] fSSegmentMap;
  }

  if (fOutputFile) {
    fOutputFile->Write();
    fOutputFile->Close();
    delete fOutputFile;
  }

  if (fFOCAL)
    delete fFOCAL;

  if (fFOCALLoader)
    delete fFOCALLoader;

  if (fRunLoader)
    delete fRunLoader;
}

//____________________________________________________________________________
void AliFOCALDigitizer::Digitize(Option_t* /*option*/) {
  AliDebug(1,"AliFOCALDigitizer::Digitize called with option.");
}

//____________________________________________________________________________
void AliFOCALDigitizer::Initialize(Option_t * option) {

  SetOption(option);
  
  if (!fGeom) {
    AliError("AliFOCALDigitizer::Initialize(Option * option): You need to set geometry before calling this method!");
    return;
  }
  
  InitializeArraysAndMaps();
}

//____________________________________________________________________________
void AliFOCALDigitizer::Initialize(const char * pathToGalice, const char * geometryFile, Option_t *option)
{
  // Loads galice.root file and corresponding header, kinematics
  // hits and sdigits or digits depending on the option
  //
  
  if (fRunLoader) {
    AliError("AliFOCALDigitizer::InitializeLocal: Already initialized!");
    return;
  }

  TString evfoldname = AliConfig::GetDefaultEventFolderName();
  fRunLoader = AliRunLoader::GetRunLoader(evfoldname);
  if (!fRunLoader) {
    fRunLoader = AliRunLoader::Open(pathToGalice,AliConfig::GetDefaultEventFolderName(), "UPDATE");
  }
  
  if (!fRunLoader) {
    AliError(Form("Can not open session for file %s.",pathToGalice));
  }

  SetOption(option);
  
  if (fDoDigits || fDoSDigits) {
    if (!fRunLoader->GetAliRun()) {
      fRunLoader->LoadgAlice();
    }
    if (!fRunLoader->TreeE()) {
      fRunLoader->LoadHeader();
    }
    if (!fRunLoader->TreeK()) {
      fRunLoader->LoadKinematics();
    }
  
    gAlice = fRunLoader->GetAliRun();
  
    if (gAlice) {
      AliDebug(1,"Alirun object found");
    }
    else {
      AliError("Could not found Alirun object");
    }
  
    fFOCAL = (AliFOCAL*)gAlice->GetDetector("FOCAL");
    fGeom = AliFOCALGeometry::GetInstance(geometryFile);
  }

  fFOCALLoader = fRunLoader->GetLoader("FOCALLoader");
  if (fFOCALLoader == 0x0) {
    AliError("Can not find FOCALLoader");
  }
  if (fDoDigits || fDoSDigits) {
    fFOCALLoader->LoadHits("READ");
  }
  if (fDoSDigits) {
    fFOCALLoader->LoadSDigits("recreate");
  }
  if (fDoDigits) {
    fFOCALLoader->LoadDigits("recreate");
  }
  
  fLocalMode = false;
  InitializeArraysAndMaps();
}

//____________________________________________________________________________
void AliFOCALDigitizer::InitializeLocal(const char * pathToGalice, const char * geometryFile, const char * outputFile, Option_t *option)
{
  // Loads galice.root file and corresponding header, kinematics
  // hits and sdigits or digits depending on the option
  //
  if (fRunLoader) {
    AliError("AliFOCALDigitizer::InitializeLocal: Already initialized!");
    return;
  }
  
  fRunLoader = AliRunLoader::Open(pathToGalice);
  
  if (!fRunLoader) {
    AliError(Form("Can not open session for file %s.",pathToGalice));
  }

  SetOption(option);
  
  if(fDoDigits || fDoSDigits)
  {
    if (!fRunLoader->GetAliRun()) {
      fRunLoader->LoadgAlice();
    }
    if (!fRunLoader->TreeE()) {
      fRunLoader->LoadHeader();
    }
    if (!fRunLoader->TreeK()) {
      fRunLoader->LoadKinematics();
    }
  
    gAlice = fRunLoader->GetAliRun();
    if (gAlice) {
      AliDebug(1,"Alirun object found");
    }
    else {
      AliError("Could not found Alirun object");
    }
  
    fFOCAL = (AliFOCAL*)gAlice->GetDetector("FOCAL");
    fGeom = AliFOCALGeometry::GetInstance(geometryFile);
  }

  fFOCALLoader = fRunLoader->GetLoader("FOCALLoader");
  if (fFOCALLoader == 0x0) {
    AliError("Can not find FOCALLoader");
  }

  if (fDoDigits || fDoSDigits) {
    fFOCALLoader->LoadHits("READ");
    fOutputFile = new TFile(outputFile,"RECREATE");
  }
  
  fLocalMode = true;  
  InitializeArraysAndMaps();
}

//____________________________________________________________________________
void AliFOCALDigitizer::InitializeArraysAndMaps() {
  
  // Get number of collumns, rows and layers
  int nlayers = fGeom->GetNumberOfLayers();  
  // Initialize SDigits if needed
  if (fDoSDigits) {
    if (fSDigits || fSSegmentMap) {
      AliDebug(1,"SDigit objects not empty, please clear them before calling init");
      return;
    }
    cout << "Initializing fSDigits" << endl;
    fSDigits = new TClonesArray("AliFOCALdigit", 1000);
    fNSDigit = 0;
    
    fSSegmentMap = new AliFOCALSegmentMap [nlayers];
    Int_t cols,rows,seg;
    for (Int_t i = 0; i < nlayers ; i++) {
      fGeom->GetVirtualSegmentFromLayer(i,seg);      
      cout << "\tlayer " << i << endl;
      fGeom->GetVirtualNColRow(seg,cols,rows);
      fSSegmentMap[i].CreateMap(i,cols,rows);
    } 
  }
  
  // Initialize Digits if needed
  if (fDoDigits) {
    if (fDigits || fSegmentMap) {
      AliDebug(1,"Digit objects not empty, please clear them before calling init");
      return;
    }
    cout << "Initializing fDigits" << endl;
    fDigits = new TClonesArray("AliFOCALdigit", 1000);
    fNDigit = 0;
    
    fSegmentMap = new AliFOCALSegmentMap [nlayers];
    Int_t cols,rows,seg;
    for (Int_t i = 0; i < nlayers ; i++) {
      fGeom->GetVirtualSegmentFromLayer(i,seg);            
      fGeom->GetVirtualNColRow(seg,cols,rows);
      //cout << "Making layer maps; layer " << i << " cols " << cols << " " << rows << endl;
      fSegmentMap[i].CreateMap(i,cols,rows);
    } 
  }
}

//____________________________________________________________________________
TClonesArray* AliFOCALDigitizer::GetSegmentDigits() const {
  //
  // Sum digits from the same (col,row) cell over the whole segment
  //
  if (!fDigits || !fSegmentMap) {
    cout << "AliFOCALDigitizer::GetSegmentDigits(): Digits or digit maps not created! Run the digitization first" << endl;
    return 0x0;
  }
  TClonesArray* segmentDigits = new TClonesArray("AliFOCALdigit", 1000);
  int ndigits = 0;
  
  for (int iseg=0; iseg<fGeom->GetVirtualNSegments(); iseg++) {
    int cols,rows;      
    fGeom->GetVirtualNColRow(iseg,cols,rows);
    //cout << "################################ GetSegmentDigits() iseg: " << iseg << endl;
    for (int index = 0; index < cols*rows; index++) {
      AliFOCALdigit* digit = 0x0;
      for (int ilayer = fGeom->GetVirtualMinLayerInSegment(iseg); ilayer<=fGeom->GetVirtualMaxLayerInSegment(iseg); ilayer++) {
        AliFOCALdigit* layerDigit = fSegmentMap[ilayer].fDigitMap[index];
        if (!layerDigit) {
          continue;      
        }
        if (!digit) {              // initialize the digit
          digit = new ((*segmentDigits)[ndigits]) AliFOCALdigit(*layerDigit);
          //digit = (AliFOCALdigit*)segmentDigits->AddrAt(ndigits);
          ndigits++;
          //cout << "found digit col,row,layer, depEn :: " << layerDigit->GetCol() << ", " << layerDigit->GetRow() << ", "
          //<< layerDigit->GetLayer() << ", " << layerDigit->GetDepEn() << endl; 
        } else {  // sum digits from the layers in the same segment
          digit->IncreaseDepEn(layerDigit->GetDepEn());
          digit->IncreaseAmp(layerDigit->GetAmp());
          //cout << "increase digit col,row,layer, depEn :: " << layerDigit->GetCol() << ", " << layerDigit->GetRow() << "," 
          //<< layerDigit->GetLayer() << ", " << layerDigit->GetDepEn() << endl;
        }
      }  // end loop over layers in a segment
    }  // end loop over (cols,rows)
  }  // end loop over virtual segments
  
  return segmentDigits;
}

//____________________________________________________________________________
AliFOCALdigit* AliFOCALDigitizer::GetDigit(Int_t col, Int_t row, Int_t layer) {
  //
  // Find the digit at (col,row). If it does not exist, it is created with amp = 0 and the pointer to it is returned
  // Also, the digit pointer is added to the current layer map
  Int_t nCol, nRow, seg;
  fGeom->GetVirtualSegmentFromLayer(layer,seg);
  fGeom->GetVirtualNColRow(seg,nCol,nRow);
  
  if ( (col >= nCol) || (row >= nRow) || (seg >= fGeom->GetVirtualNSegments()) || 
       (col < 0) || (row < 0) || (fGeom->GetVirtualNSegments() < 0) ) {
    AliDebug(1,Form("AliFOCALDigitizer::GetDigit: indexes %i,%i,%i out of bounds",col,row,seg));
    cout << "AliFOCALDigitizer::GetDigit() out of bounds, col, row, seg " << col << ", " << row << ", " << seg << endl;
    return 0;
  }
  
  Long_t index = row*nCol + col;
  if (!fSegmentMap[layer].fDigitMap[index]) {
    new ((*fDigits)[fNDigit]) AliFOCALdigit(col,row,layer,0);
    fSegmentMap[layer].fDigitMap[index] = (AliFOCALdigit*)fDigits->AddrAt(fNDigit);
    fSegmentMap[layer].fDigitMap[index]->SetSegment(seg);
    fNDigit++;
  }    
  return fSegmentMap[layer].fDigitMap[index];
}

//____________________________________________________________________________
AliFOCALdigit* AliFOCALDigitizer::GetSDigit(Int_t col, Int_t row, Int_t layer) {
  //
  // Find the summable digit at (col,row). If it does not exist, it is created with amp = 0 and the pointer to it is returned
  // Also, the digit pointer is added to the current layer map
  Int_t nCol, nRow, seg;
  fGeom->GetVirtualSegmentFromLayer(layer,seg);
  fGeom->GetVirtualNColRow(seg,nCol,nRow);
  
  if ( (col >= nCol) || (row >= nRow) || (seg >= fGeom->GetVirtualNSegments()) || 
       (col < 0) || (row < 0) || (fGeom->GetVirtualNSegments() < 0) ) {
    AliDebug(1,Form("AliFOCALDigitizer::GetDigit: indexes %i,%i,%i out of bounds",col,row,seg));
    return 0;
  }
  
  Long_t index = row*nCol + col;
  if (!(fSSegmentMap[layer].fDigitMap[index])) {
    new ((*fSDigits)[fNSDigit]) AliFOCALdigit(col,row,layer,0);
    fSSegmentMap[layer].fDigitMap[index] = (AliFOCALdigit*)fSDigits->AddrAt(fNSDigit);
    fSSegmentMap[layer].fDigitMap[index]->SetSegment(seg);
    fNSDigit++;
  }    
  return fSSegmentMap[layer].fDigitMap[index];
}

//____________________________________________________________________________
Bool_t AliFOCALDigitizer::RemoveDigit(AliFOCALdigit* digit) {
  //  
  // Remove Digit from the map
  if (!digit)
    return kFALSE;
  
  Int_t col = digit->GetCol();
  Int_t row = digit->GetRow();
  Int_t seg = digit->GetSegment();
  Int_t layer = digit->GetLayer();
  Int_t nCol, nRow;
  fGeom->GetVirtualNColRow(seg,nCol,nRow);
  
  if ( (col >= nCol) || (row >= nRow) || (seg >= fGeom->GetVirtualNSegments()) || 
       (col < 0) || (row < 0) || (fGeom->GetVirtualNSegments() < 0) ) {
    AliDebug(1,Form("AliFOCALDigitizer::RemoveDigit: Digit at %i,%i,%i is out of bounds",col,row,seg));
    cout << "AliFOCALDigitizer::RemoveDigit() out of bounds, col, row, seg " << col << ", " << row << ", " << seg << endl;
    return kFALSE;
  }
  
  Long_t index = row*nCol + col;
  if (!fSegmentMap[layer].fDigitMap[index]) {
    AliDebug(1,Form("AliFOCALDigitizer::RemoveDigit: Digit at %i,%i,%i is not in the map",col,row,layer));
    cout << "AliFOCALDigitizer::RemoveDigit() not in map, col, row, layer " << col << ", " << row << ", " << layer << endl;
    return kFALSE;
  } else {
    fSegmentMap[layer].fDigitMap[index] = 0;  // !! TODO: this is a memory leak, we should delete the digit first
    return kTRUE;
  }
}

//____________________________________________________________________________
Bool_t AliFOCALDigitizer::RemoveSDigit(AliFOCALdigit * digit) {
  //
  // Remove S-Digit from the map
  if (!digit)
    return kFALSE;
  
  Int_t col = digit->GetCol();
  Int_t row = digit->GetRow();
  Int_t seg = digit->GetSegment();
  Int_t layer = digit->GetLayer();
  Int_t nCol, nRow;
  fGeom->GetVirtualNColRow(seg,nCol,nRow);
  
  if ( (col >= nCol) || (row >= nRow) || (seg >= fGeom->GetVirtualNSegments()) || 
       (col < 0) || (row < 0) || (fGeom->GetVirtualNSegments() < 0) ) {
    AliDebug(1,Form("AliFOCALDigitizer::RemoveSDigit: Digit at %i,%i,%i is out of bounds",col,row,layer));
    return kFALSE;
  }
  
  Long_t index = row*nCol + col;
  if (!fSSegmentMap[layer].fDigitMap[index]) {
    AliDebug(1,Form("AliFOCALDigitizer::RemoveSDigit: Digit at %i,%i,%i is not in the map",col,row,layer));
    return kFALSE;
  } else {
    fSSegmentMap[layer].fDigitMap[index] = 0;  // !! TODO: this is a memory leak, we should delete the digit first
    return kTRUE;
  }
}

//____________________________________________________________________________
Int_t AliFOCALDigitizer::EnergyToAmplitude(Float_t energy, Int_t segment) {
  //
  // Convert deposited energy to signal units in Digits
  // Includes gaussian noise for Pads; noise for pixels is generated elsewhere
  // The noise gaussian width is user provided in the clusterization steering macro.
  //
  bool isPixel = fGeom->GetVirtualIsPixel(segment);
  Float_t relThickness = fGeom->GetVirtualRelativeSensitiveThickness(segment);
  // TODO: The virtual pixel threshold should be changed since the values in geometry.txt file corespond to segment digits
  Float_t pixelTreshold = fGeom->GetVirtualPixelTreshold(segment);  
  Float_t rescaledEnergy;
    
  // Rescaling to correct thickness of sensitive material
  // Fluctuations TODO!!!
  rescaledEnergy = energy * relThickness;
  
  // Currently returns energy in keV
  if (!isPixel) {
    // TODO: 1.10.2022 Check the noise since now we move to digits per layer.
    rescaledEnergy += gRandom->Gaus(0, fPadNoiseSigma);  // Add noise
    if (rescaledEnergy >= pixelTreshold) {
      return (Int_t)(rescaledEnergy/1000);
    }
    else {
      return 0;
    }
  }
  else {
    if (rescaledEnergy >= pixelTreshold) {
      return 1;
    }
    return 0;
  }
}

//____________________________________________________________________________
Int_t AliFOCALDigitizer::EnergyToSAmplitude(Float_t energy, Int_t segment) {
  //
  // Convert deposited energy to signal units in Digits
  //
  bool isPixel = fGeom->GetVirtualIsPixel(segment);
  Float_t relThickness = fGeom->GetVirtualRelativeSensitiveThickness(segment);
  Float_t pixelTreshold = fGeom->GetVirtualPixelTreshold(segment);
  Float_t rescaledEnergy;
    
  // Rescaling to correct thickness of sensitive materital
  // Fluctuations TODO!!!
  rescaledEnergy = energy * relThickness;
  
  // Currently returns energy in keV
  if (!isPixel) {
    return (Int_t)(rescaledEnergy/1000);
  }
  else {
    return (rescaledEnergy >= pixelTreshold) ? 1 : 0;
  }
}

//____________________________________________________________________________
void AliFOCALDigitizer::Hits2Digits(TBranch* branchH) {
  //
  //  Main function called from outside, to trigger the creation of digits
  //
  cout << "AliFOCALDigitizer::Hits2Digits called." << endl;
  if (!branchH) {
    AliError("AliFOCALDigitizer::Hits2Digits: branchH == 0, returning!");
    return;
  }
    
  Hits2DigitsPadsPixelsOrEmbedding(branchH, 0);
}

//____________________________________________________________________________
void AliFOCALDigitizer::Hits2DigitsEvent(Int_t ievt) {
  //
  //  Main function called from outside, to trigger the creation of digits
  //  This function handles also the creation of the digits branch for the event
  cout << "Hits2Digits called on event " << ievt << endl;

  // load event
  fRunLoader->GetEvent(ievt);
  TTree* treeH = fFOCALLoader->TreeH();
  
  AliDebug(1,Form("Number of Tracks in the TreeH = %lld", treeH->GetEntries()));
  
  if ( fLocalMode && (fDoDigits || fDoSDigits)) {
    fOutputFile->cd();
    gDirectory->mkdir(Form("Event%d",ievt));
  }

  // initialize fDigits and corresponding maps/TTrees...
  if (fDoDigits) {
    if (!fLocalMode) {
      fTreeD = fFOCALLoader->TreeD();
      if (fTreeD == 0x0) {
        fFOCALLoader->MakeTree("D");
        fTreeD = fFOCALLoader->TreeD();
      }
    } else {
      fOutputFile->cd();
      gDirectory->cd(Form("Event%d",ievt));
      fTreeD = new TTree("TreeD","TreeD");
    } 
    fTreeD->Branch("FOCAL", fDigits, 16000);
  }  
  // initialize fSDigits and corresponding maps/TTrees...
  if (fDoSDigits) {
    if (!fLocalMode) {
      fTreeS = fFOCALLoader->TreeS();
      if (fTreeS == 0x0) {
        fFOCALLoader->MakeTree("S");
        fTreeS = fFOCALLoader->TreeS();
      }
    } else {
      fOutputFile->cd();
      gDirectory->cd(Form("Event%d",ievt));
      fTreeS = new TTree("TreeS","TreeS");
    } 
    fTreeS->Branch("FOCAL", fSDigits, 16000);
  }
  
  // load hits
  Hits2Digits(treeH->GetBranch("FOCAL"));
}

//____________________________________________________________________________
void AliFOCALDigitizer::Hits2DigitsEmbedding(TBranch * branchHits, TBranch * branchBackground) {
  //
  // General method for embedding, calls embedding for Pixels/Pads as necessary
  //
  cout << "AliFOCALDigitizer::Hits2DigitsEmbedding called." << endl;
  
  if ((!branchHits) || (!branchBackground)) {
    AliError("AliFOCALDigitizer::Hits2Digits: branchHits == 0 or branchBackground == 0, returning!");
    return;
  }
  
  Hits2DigitsPadsPixelsOrEmbedding(branchHits, branchBackground);
}

//____________________________________________________________________________
void AliFOCALDigitizer::Hits2DigitsPadsPixelsOrEmbedding(TBranch* branchHits, TBranch* branchBackground) {
  //
  // Embedding for Pixels
  //
  if (fDoSDigits) {
    ResetSDigit();
  }
  if (fDoDigits) {
    ResetDigit();
  }
  
  // Load branches into TCloneArrays
  TClonesArray* hits = 0;
  branchHits->SetAddress(&hits);
  
  TClonesArray* embedded = new TClonesArray("AliFOCALhit",1000);
  
  // Load All hits into 'embedded' TClonesArray
  for (Int_t iTrack=0; iTrack<branchHits->GetEntries(); iTrack++) {
    branchHits->GetEntry(iTrack); // load iTrack
    
    // NOTE: Get hits from pad segments only and make digits.
    //       The corresponding hits will be removed from the hits tree
    int nfocal = hits->GetEntries();
    // loop over hits
    for (Int_t ifocal = 0; ifocal < nfocal; ifocal++) {
      AliFOCALhit * fFOCALHit = (AliFOCALhit*) hits->UncheckedAt(ifocal);
      Float_t x, y, z;
      x = (Float_t) fFOCALHit->X();
      y = (Float_t) fFOCALHit->Y();
      z = (Float_t) fFOCALHit->Z();
        
      Int_t col,row,layer,seg;
      if (!fGeom->GetVirtualInfo(x,y,z,col,row,layer,seg)) {
        hits->RemoveAt(ifocal);
        continue;
      }
  
      if (fGeom->GetVirtualIsPixel(seg)) {
        continue;
      }

      // deposited energy in the pixel in eV
      Float_t energy = fFOCALHit->GetEnergy();
      AliFOCALdigit* digit = 0x0;
      if (fDoDigits) {
        digit = GetDigit(col,row,layer);
      }
      if (fDoSDigits) {
        digit = GetSDigit(col,row,layer);
      }
      if (digit) {
        digit->IncreaseDepEn(energy);
      }
      hits->RemoveAt(ifocal);
    } // Hits Loop ended
	  hits->Compress();
    embedded->AbsorbObjects(hits,0,hits->GetLast()); // add all the hits to embedded tclonesarray
//    delete hits; // sometimes causes segfault, not sure why
  }
  delete hits;
  
  if (branchBackground) {
  
    TClonesArray* background = 0;
    Int_t nTracksBackground = branchBackground->GetEntries();
    branchBackground->SetAddress(&background);
    
    for (Int_t track=0; track<nTracksBackground;track++) {
      branchBackground->GetEntry(track); // load track
      // Sort out hits from 'pad' segmets
      int nfocal = background->GetEntries();
      // loop over hits
      for (Int_t ifocal = 0; ifocal < nfocal; ifocal++) {
        AliFOCALhit* fFOCALHit = (AliFOCALhit*) background->UncheckedAt(ifocal);
        Float_t x, y, z;
        x = (Float_t) fFOCALHit->X();
        y = (Float_t) fFOCALHit->Y();
        z = (Float_t) fFOCALHit->Z();

        Int_t col,row,layer,seg;
        if (!fGeom->GetVirtualInfo(x,y,z,col,row,layer,seg)) {
          background->RemoveAt(ifocal);
          continue;
        }
        if (fGeom->GetVirtualIsPixel(seg)) {
          continue;
        }
        // deposited energy in the pixel in eV
        Float_t energy = fFOCALHit->GetEnergy();
        AliFOCALdigit* digit = 0x0;
        if (fDoDigits) {
          digit = GetDigit(col,row,layer);
        }
        if (fDoSDigits) {
          digit = GetSDigit(col,row,layer);
        }
        
        if (digit) {
          digit->IncreaseDepEn(energy);
        }

        background->RemoveAt(ifocal);
      } // Hits Loop ended

      background->Compress();
      embedded->AbsorbObjects(background,0,background->GetLast()); // add all the hits to embedded tclonesarray
      //    delete background; // sometimes causes segfault, not sure why
    }
    delete background;
  }
  
  // All pad digits are summed, we now need to apply thresholds and calculate output signal
  if (fDoDigits) {
    for (Int_t d = 0; d < fNDigit; d++) {
      AliFOCALdigit* dig = (AliFOCALdigit*) fDigits->UncheckedAt(d);
      Int_t signal = EnergyToAmplitude(dig->GetDepEn(),dig->GetSegment());
      if (signal > 0) {
        dig->SetAmp(signal);
      } else {
        // Digit would have zero signal, remove it from the map:
        RemoveDigit(dig);
        fDigits->RemoveAt(d);
      }
    }
    fDigits->Compress();
    fNDigit = fDigits->GetEntries();
  }

  // All pad sdigits are summed, we now need to apply thresholds and calculate output signal
  if (fDoSDigits) {
    for (Int_t d = 0; d < fNSDigit; d++) {
      AliFOCALdigit * dig = (AliFOCALdigit*) fSDigits->UncheckedAt(d);
      Int_t signal = EnergyToSAmplitude(dig->GetDepEn(),dig->GetSegment());
      if (signal > 0) {
        dig->SetAmp(signal);
      } else {
        // Digit would have zero signal, remove it from the map:
        RemoveSDigit(dig);
        fSDigits->RemoveAt(d);
      }
    }
    fSDigits->Compress();
    fNSDigit = fSDigits->GetEntries();
  }
    
  Int_t padDigits = fNDigit;
  Int_t padSDigits = fNSDigit;

  // Compute indices based on the XY position in the plane for the remaining hits (from pixel planes).
  // The hits will then be soretd according to the indices and this will in turn be used to optimize the creation of the digits from pixels
  Float_t XSize = fGeom->GetFOCALSizeX();
  Float_t YSize = fGeom->GetFOCALSizeY();
  Int_t XTot = Int_t(XSize/fGeom->GetGlobalPixelSize() + 0.001);
  Int_t YTot = Int_t(YSize/fGeom->GetGlobalPixelSize() + 0.001);
  Int_t embeddedEntries = embedded->GetEntries();
  AliFOCALhit * hit = 0x0;
  for (Int_t i = 0; i < embeddedEntries; i++) {
    hit = (AliFOCALhit*) embedded->UncheckedAt(i);
    
    Int_t segment, layer;
    fGeom->GetVirtualLayerSegment(hit->Z(), layer, segment);
    if (segment < 0) {
      embedded->Remove(hit);
      continue;
    }  
    Int_t x = (Int_t) ((hit->X() + XSize/2)/XSize*XTot);
    Int_t y = (Int_t) ((hit->Y() + YSize/2)/YSize*YTot);
    hit->SetIndex((ULong64_t)((ULong64_t)(layer)*(ULong64_t)(XTot)*(ULong64_t)(YTot) + (ULong64_t)(x)*(ULong64_t)(YTot) + (ULong64_t)(y)));
  }
  // Sort Hits according to the indexes
  embedded->Compress();
  embedded->Sort();
  
  // Loop over the hits to create digits (macro-pixels)
  Int_t curLayer = -1;
  Int_t curPixCol  = -1;
  Int_t curPixRow  = -1;
  Int_t newLayer = -1;
  ULong64_t newLayerIndex = 0;
  Int_t newPixCol  = -1;
  Int_t newPixRow  = -1;
  Float_t energy = 0;
  AliFOCALdigit * digit;
  Float_t pixelPadRatio;
  embeddedEntries = embedded->GetEntries();
  for (Int_t i = 0; i < embeddedEntries; i++) {
    hit = (AliFOCALhit*) embedded->UncheckedAt(i);
    // recover the hit coordinates from the index
    newLayer = (Int_t)(hit->GetIndex() / ((ULong64_t)(XTot)*(ULong64_t)(YTot)));
    newLayerIndex = hit->GetIndex() % ((ULong64_t)(XTot)*(ULong64_t)(YTot));
    newPixCol = (Int_t)(newLayerIndex / (ULong64_t)(YTot));
    newPixRow = (Int_t)(newLayerIndex % (ULong64_t)(YTot));
    
    // if a new (row,col,layer) cell is found, then create a digit initialized with the energy sum of all accumulated hits in the previous cell 
    if ((newPixRow != curPixRow) || (newPixCol != curPixCol) || (newLayer != curLayer)) {
      // Skip first non-existent "pixel"
      if (curLayer != -1) {
        // Calculate "pixels per pad" ratio:
        Int_t segment;      
        fGeom->GetVirtualSegmentFromLayer(curLayer,segment);
        pixelPadRatio = fGeom->GetVirtualPadSize(segment)/fGeom->GetGlobalPixelSize();
        
        if (fDoDigits) {
          digit = GetDigit(Int_t(curPixCol/pixelPadRatio),Int_t(curPixRow/pixelPadRatio),curLayer);
          if (digit)
            digit->IncreaseAmp(EnergyToAmplitude(energy,segment));
        }
        if (fDoSDigits) {
          digit = GetSDigit(Int_t(curPixCol/pixelPadRatio),Int_t(curPixRow/pixelPadRatio),curLayer);
          if (digit) {
            digit->IncreaseAmp(EnergyToSAmplitude(energy,segment));
          }
        }
      }
      
      curPixCol = newPixCol;
      curPixRow = newPixRow;
      curLayer = newLayer;
      energy = hit->GetEnergy();
    } 
    else
      energy += hit->GetEnergy();
  }
  
  // This creates the last digit since in the implementation above, the last one will always be left out
  if (curLayer != -1) {
    Int_t segment;      
    fGeom->GetVirtualSegmentFromLayer(curLayer,segment);      
    pixelPadRatio = fGeom->GetVirtualPadSize(segment)/fGeom->GetGlobalPixelSize();
    if (fDoDigits) {
      digit = GetDigit(Int_t(curPixCol/pixelPadRatio),Int_t(curPixRow/pixelPadRatio),curLayer);
      if (digit) {
        digit->IncreaseAmp(EnergyToAmplitude(energy,segment));
      }
    }
    if (fDoSDigits) {
      digit = GetSDigit((Int_t)(curPixCol/pixelPadRatio),(Int_t)(curPixRow/pixelPadRatio),curLayer);
      if (digit) {
        digit->IncreaseAmp(EnergyToSAmplitude(energy,segment));
      }
    }
  }
  
  // Add pixel noise
  if (fDoDigits) {
    for (Int_t iLayer = 0; iLayer < fGeom->GetNumberOfLayers(); iLayer++) {
      Int_t iSeg;
      fGeom->GetVirtualSegmentFromLayer(iLayer, iSeg);
      if (fGeom->GetVirtualIsPixel(iSeg)) {
        // pixels per macro-pixel
        pixelPadRatio = fGeom->GetVirtualPadSize(iSeg)/fGeom->GetGlobalPixelSize();
        Int_t nPixTot = XTot * YTot;
        Int_t nPixNoise = gRandom->Poisson(fPixelNoiseProb * nPixTot);
        for (Int_t iNoise = 0 ; iNoise < nPixNoise; iNoise++) {
          Int_t iCol = Int_t(gRandom->Rndm()*XTot);
          Int_t iRow = Int_t(gRandom->Rndm()*YTot);
          digit = GetDigit(Int_t(iCol/pixelPadRatio), Int_t(iRow/pixelPadRatio), iLayer);
          if (digit) {
            digit->IncreaseAmp(1);  // Add 1 noise hit
          }
        }
      }
    }
  }
  
  // Remove digits with 0 signal
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // TODO: This part of the code should in principle do nothing. Probably remove it, or do a real check of amplitudes.
  if (fDoDigits) {
    for (Int_t d = padDigits; d < fNDigit; d++) {
      AliFOCALdigit * dig = (AliFOCALdigit*) fDigits->UncheckedAt(d);
      if (dig->GetAmp() <= 0) {
        RemoveDigit(dig);
        fDigits->RemoveAt(d);
      }
    }
    fDigits->Compress();
    fNDigit = fDigits->GetEntries();
  }
  // Remove sdigits with 0 signal
  if (fDoSDigits) {
    for (Int_t d = padSDigits; d < fNSDigit; d++) {
      AliFOCALdigit * dig = (AliFOCALdigit*) fSDigits->UncheckedAt(d);
      if (dig->GetAmp() <= 0) {
        RemoveSDigit(dig);
        fSDigits->RemoveAt(d);
      }
    }
    fSDigits->Compress();
    fNSDigit = fSDigits->GetEntries();
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  delete embedded;  // delete the array holding the hits
}

//____________________________________________________________________________
void AliFOCALDigitizer::Hits2DigitsEmbeddingPads(TBranch * branchHits, TBranch * branchBackground) {
  //  
  // Embedding for Pads
  //
  cout << "AliFOCALDigitizer::Hits2DigitsEmbeddingPads called." << endl;
     
  TClonesArray* hits = 0;
  Int_t ntracks = branchHits->GetEntries();
  cout << "    Signal Event has " << ntracks << " tracks." << endl;
  branchHits->SetAddress(&hits);

  // loop over 'tracks' from Hits
  for (Int_t track=0; track<ntracks;track++) {
    branchHits->GetEntry(track);
    
    int nfocal = hits->GetEntries();
    // loop over hits
    for (Int_t ifocal = 0; ifocal < nfocal; ifocal++) {
      AliFOCALhit * fFOCALHit = (AliFOCALhit*) hits->UncheckedAt(ifocal);
      Float_t x, y, z;  
      x = (Float_t) fFOCALHit->X(); 
      y = (Float_t) fFOCALHit->Y(); 
      z = (Float_t) fFOCALHit->Z();

      // deposited energy in the pixel in GeV
      Float_t energy = fFOCALHit->GetEnergy();
        
      Int_t col,row,layer,seg;
      if (!fGeom->GetVirtualInfo(x,y,z,col,row,layer,seg)) {
        continue;
      }
        
      AliFOCALdigit * digit;
      if (fDoDigits) {
        digit = GetDigit(col,row,layer);
        if (digit) {
          digit->IncreaseAmp(EnergyToAmplitude(energy,seg));
        }
      }
        
      if (fDoSDigits) {
        digit = GetSDigit(col,row,layer);
        if (digit) {
          digit->IncreaseAmp(EnergyToSAmplitude(energy,seg));
        }
      }
    } // Hits Loop ended
    delete hits;
  } // Track Loop ended
  
  ntracks = branchBackground->GetEntries();
  cout << "    Background Event has " << ntracks << " tracks." << endl;
  TClonesArray* hits2 = 0;
  branchBackground->SetAddress(&hits2);
  
  // loop over 'tracks' from Background
  for (Int_t track=0; track<ntracks;track++) {
    branchBackground->GetEntry(track);
    int nfocal = hits2->GetEntries();
    // loop over hits
    for (Int_t ifocal = 0; ifocal < nfocal; ifocal++) {
      AliFOCALhit * fFOCALHit = (AliFOCALhit*) hits2->UncheckedAt(ifocal);
      Float_t x, y, z;  
      x = (Float_t) fFOCALHit->X(); 
      y = (Float_t) fFOCALHit->Y(); 
      z = (Float_t) fFOCALHit->Z();

      // deposited energy in the pixel in GeV
      Float_t energy = fFOCALHit->GetEnergy();
      Int_t col,row,layer,seg;
      if (!fGeom->GetVirtualInfo(x,y,z,col,row,layer,seg)) {
        continue;
      }     
      AliFOCALdigit * digit;
      if (fDoDigits) {
        digit = GetDigit(col,row,layer);
        if (digit) {
          digit->IncreaseAmp(EnergyToAmplitude(energy,seg));
        }
      }
      if (fDoSDigits) {
        digit = GetSDigit(col,row,layer);
        if (digit) {
          digit->IncreaseAmp(EnergyToSAmplitude(energy,seg));
        }
      }
    } // Hits Loop ended
    delete hits2;
  } // Track Loop ended
}

//____________________________________________________________________________
void AliFOCALDigitizer::StoreDigits() {

  cout << "AliFOCALDigitizer::StoreDigits called" << endl;
  if (fDoDigits) {
    if (!fSDigits || !fSSegmentMap) {
      AliDebug(1,Form("AliFOCALDigitizer::StoreDigit: not initialized"));
      return;
    }
    fTreeD->Fill();
    if (!fLocalMode) {
      fFOCALLoader->WriteDigits("OVERWRITE");
    } else {
      fTreeD->Write();
      fOutputFile->Write();
      fTreeD->Delete();
    }
  }
  
  if (fDoSDigits) {
    if (!fSDigits || !fSSegmentMap) {
      AliDebug(1,Form("AliFOCALDigitizer::StoreDigit: not initialized"));
      return;
    }
    fTreeS->Fill();
    if (!fLocalMode) {
      fFOCALLoader->WriteSDigits("OVERWRITE");
    } else {
      fTreeS->Write();
      fOutputFile->Write();
      fTreeS->Delete();
    }
  }
}

//____________________________________________________________________________
void AliFOCALDigitizer::ResetSDigit()
{
  if (!fDoSDigits) {
    return;
  }
    
  fNSDigit = 0;
  fSDigits->Clear();
  for (Long_t i = 0; i < fGeom->GetNumberOfLayers(); i++) {
    fSSegmentMap[i].ResetMap();
  }
}

//____________________________________________________________________________
void AliFOCALDigitizer::ResetDigit()
{
  if (!fDoDigits) {
    return;
  }
    
  fNDigit = 0;
  fDigits->Clear();
  for (Long_t i = 0; i < fGeom->GetNumberOfLayers(); i++) {
    fSegmentMap[i].ResetMap();
  }
}

//____________________________________________________________________________
void AliFOCALDigitizer::SetOption(Option_t * option) {

  if (strstr(option,"HS")) { 
    fDoSDigits = true; 
  }
  else {
    fDoSDigits = false;
  }
    
  if (strstr(option,"HD")) {
    fDoDigits = true;
  }
  else { 
    fDoDigits = false;
  }
}
