#ifndef ALIFOCALKinematics_H
#define ALIFOCALKinematics_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */
//-----------------------------------------------------//
//                                                     //
//                                                     //
//  Date   : July  30 2009                             //
//                                                     //
//  Utility class for FOCAL                            //
//                                                     //
//-----------------------------------------------------//
// Author - T. Gunji
//

#include "Rtypes.h"
#include "TObject.h"
class TClonesArray;


class AliFOCALKinematics : public TObject{

public:
  AliFOCALKinematics();
  AliFOCALKinematics(Int_t event, Int_t pid, float px, float py, float pz,
		     float rap, float eta, int mother, int acc);

  AliFOCALKinematics(AliFOCALKinematics *fKinematics);
  AliFOCALKinematics (const AliFOCALKinematics &fKinematics);  // copy constructor
  AliFOCALKinematics &operator=(const AliFOCALKinematics &fKinematics); // assignment op

  virtual ~AliFOCALKinematics();

  virtual Int_t Pid() const{ return fPid;}
  virtual Int_t Acc() const{ return fAcc;}
  virtual Int_t Mother() const{ return fMother;}
  virtual Float_t Px() const{ return fPx;}
  virtual Float_t Py() const{ return fPy;}
  virtual Float_t Pz() const{ return fPz;}
  virtual Float_t Rap() const{ return fRap;}
  virtual Float_t Eta() const{ return fEta;}

  Int_t fEvent; 
  Int_t fPid;
  Float_t fPx;
  Float_t fPy;
  Float_t fPz;
  Float_t fRap;
  Float_t fEta;
  Int_t   fMother;
  Int_t   fAcc;
  

  ClassDef(AliFOCALKinematics,4) // Utility class for the detector set:FOCAL
    };

#endif
