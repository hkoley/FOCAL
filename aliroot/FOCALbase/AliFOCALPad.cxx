/***************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/
//-----------------------------------------------------//
//                                                     //
//                                                     //
//  Date   : July 30   2009                            //
//                                                     //
//  Pad code for ALICE-FOCAL                          //
//                                                     //
//-----------------------------------------------------//


#include "Riostream.h"
#include "TMath.h"
#include "TText.h"
#include "TLine.h"

#include <stdio.h>
#include <math.h>

#include "AliFOCALPad.h"


ClassImp(AliFOCALPad)

AliFOCALPad::AliFOCALPad():
  TObject(),
  fPadid(0),
  fRow(0),
  fCol(0),
  fLayer(0),
  fSegment(0),
  fTower(0),
  fX(0),
  fY(0),
  fZ(0),
  fE(0),
  fIsPixel(false)
{
  //Default constructor
}

AliFOCALPad::AliFOCALPad(Int_t pad, Int_t row, Int_t col, Int_t layer, Int_t segment, Int_t tower, Float_t x, Float_t y, Float_t z, Float_t e):
	TObject(),
	fPadid(pad),
	fRow(row),
	fCol(col),
	fLayer(layer),
	fSegment(segment),
	fTower(tower),
	fX(x),
	fY(y),
	fZ(z),
	fE(e),
	fIsPixel(false)
{
	//Default constructor
}

AliFOCALPad::AliFOCALPad(Int_t pad, Int_t row, Int_t col, Int_t layer, Int_t segment, Int_t tower, Float_t x, Float_t y, Float_t z, Float_t e, bool isPixel):
	TObject(),
	fPadid(pad),
	fRow(row),
	fCol(col),
	fLayer(layer),
	fSegment(segment),
	fTower(tower),
	fX(x),
	fY(y),
	fZ(z),
	fE(e),
	fIsPixel(isPixel)
{
	//Default constructor
}

AliFOCALPad::AliFOCALPad(AliFOCALPad *fPad):
	TObject(),
	fPadid(0),
	fRow(0),
	fCol(0),
	fLayer(0),
	fSegment(0),
	fTower(0),
	fX(0),
	fY(0),
	fZ(0),
	fE(0),
	fIsPixel(false)
{
	*this = fPad; 
}

AliFOCALPad::AliFOCALPad(const AliFOCALPad &fPad):
	TObject(fPad),
	fPadid(fPad.fPadid),
	fRow(fPad.fRow),
	fCol(fPad.fCol),
	fLayer(fPad.fLayer),
	fSegment(fPad.fSegment),
	fTower(fPad.fTower),
	fX(fPad.fX),
	fY(fPad.fY),
	fZ(fPad.fZ),
	fE(fPad.fE),
	fIsPixel(fPad.fIsPixel)
{
}

AliFOCALPad & AliFOCALPad::operator=(const AliFOCALPad &fPad)
{
	if(this != &fPad)
	{
		fPadid=fPad.fPadid;
		fRow = fPad.fRow;
		fCol = fPad.fCol;
		fLayer = fPad.fLayer;
		fSegment = fPad.fSegment;
		fTower = fPad.fTower;
		fX = fPad.fX;
		fY = fPad.fY;
		fZ = fPad.fZ;
		fE = fPad.fE;
		fIsPixel = fPad.fIsPixel;
	}
	return *this;
}

AliFOCALPad::~AliFOCALPad()
{
  // Default destructor
}


