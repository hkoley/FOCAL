#ifndef ALIFOCALRinger_H
#define ALIFOCALRinger_H

#include "Rtypes.h" // for Int_t etc

class AliFOCALRinger {
	
	 public:
	  
	  AliFOCALRinger();
	  AliFOCALRinger(Int_t radius);
	  virtual ~AliFOCALRinger();
	  
	 public: 
	 
	  bool Init(Int_t maxRadius);
	  bool SetRing(Int_t radius);
	  bool GetNext(Int_t & x, Int_t & y);
	  
	 private:
    

    Int_t fPosition;
    Int_t fRadius;
    Int_t fMaxRadius;
    Int_t * * fRingCoordinates;
    Int_t * fRingSizes;
	  
	  ClassDef(AliFOCALRinger,3);  // Clusterization algorithm class
};
#endif // AliFOCALLCLUSTERIZER_H



