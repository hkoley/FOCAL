#ifndef ALIFOCALDIGIT_H
#define ALIFOCALDIGIT_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */
//-----------------------------------------------------//
//                                                     //
//  Date   : August 03 2009                            //
//                                                     //
//  Store digits  for FOCAL                              //
//                                                     //
//-----------------------------------------------------//

#include "TObject.h"
#include "AliDigitNew.h"

class TClonesArray;

class AliFOCALdigit : public AliDigitNew
{
 public:
  AliFOCALdigit();
  AliFOCALdigit(Int_t id, Int_t index, Int_t col, Int_t row, Int_t layer, Int_t amp);
  AliFOCALdigit(Int_t col, Int_t row, Int_t layer, Int_t amp);
  AliFOCALdigit(AliFOCALdigit *focaldigit);
  AliFOCALdigit (const AliFOCALdigit &focaldigit);  // copy constructor
  AliFOCALdigit &operator=(const AliFOCALdigit &focaldigit); // assignment op

  virtual ~AliFOCALdigit();

  virtual void SetCol(Int_t id){fCol = id;}
  virtual void SetRow(Int_t id){fRow = id;}
  virtual void SetLayer(Int_t id){fLayer = id;}
  virtual void SetSegment(Int_t id){fSegment = id;}
  virtual void SetAmp(Int_t val){fAmp = val;}
  virtual void SetDepEn(Float_t val){fDepEn = val;}
  virtual void SetId(Int_t id) {fId = id;}
  virtual void SetIndexInList(Int_t index) {fIndexInList = index;}
  
  virtual void IncreaseAmp(Int_t val){fAmp += val;}
  virtual void IncreaseDepEn(Float_t val){fDepEn += val;}

  virtual Int_t GetCol() const {return fCol;}
  virtual Int_t GetRow() const {return fRow;}
  virtual Int_t GetLayer() const {return fLayer;}
  virtual Int_t GetSegment() const {return fSegment;}
  virtual Float_t GetDepEn() const {return fDepEn;}
  // GetAmp, GetId and GetIndexInList are inherited from base class

 protected:
  Int_t fCol ;    // global collumn in the whole detector
  Int_t fRow ;    // global row in the whole detector
  Int_t fLayer;   // layer number
  Int_t fSegment; // segment of the detector
  Float_t fDepEn; // deposited energy in the pad/pixel
  
  ClassDef(AliFOCALdigit,6) // Digits object for Detector set:FOCAL
};

#endif
