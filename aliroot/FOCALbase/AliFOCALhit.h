#ifndef ALIFOCALHIT_H
#define ALIFOCALHIT_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */

////////////////////////////////////////////////
//              hits classes for set:FOCAL      //
////////////////////////////////////////////////
 
#include "AliHit.h"
#include "Riostream.h"

class TClonesArray;

class AliFOCALhit : public AliHit {

 public:
  AliFOCALhit();
  AliFOCALhit(Int_t shunt, Int_t track, Int_t *vol, Float_t *hits);
  AliFOCALhit(AliFOCALhit* oldhit);
  virtual ~AliFOCALhit() {}
  Int_t   GetVolume(Int_t i) const {return fVolume[i];}
  Float_t GetEnergy() const {return fEnergy;}
  Float_t GetTime() const {return fTime;}
  ULong64_t GetIndex() const {return fIndex;}
  
  void SetIndex(ULong64_t index) {fIndex = index; }

  // For Sorting
  Int_t Compare(const TObject * obj) const;
  bool IsSortable() const {return true;}

  //Float_t GetTime(Int_t i) const {return fTime[i];}
  //Float_t GetEnergy(Int_t i) const {return fEnergy[i];}
  
  Int_t operator == (AliFOCALhit &cell) const;
  AliFOCALhit operator + (AliFOCALhit &cell) {
    //  fEnergySum+=cell.GetEnergySum();
    fEnergy+=cell.GetEnergy();
    return *this;
  }
  //  void Print(void) const {
  //    printf("FOCAL Cell %d %d\n   Primary %d -   Energy %f\n",
  //	   fVolume[0],fVolume[1],fVolume[2],fTrack,fEnergy);
  //  }

 

 protected:
  
  ULong64_t   fIndex;
  Int_t   fVolume[4];    //array of volumes
  //Float_t fEnergySum;       //Total energy deposited in Ve
  Float_t fEnergy;      //Total energy deposited in Ve
  Float_t fTime; 
  
  //Int_t   fNhit;
  //Float_t *fEnergy;         //[fNHit] of energy deposition 
  //Float_t *fTime;         //[fNhit] of time information for the event (pile-up cal)
  

  ClassDef(AliFOCALhit,6)  //Hits object for set:FOCAL
};
#endif
