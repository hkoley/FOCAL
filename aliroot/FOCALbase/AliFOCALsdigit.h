#ifndef ALIFOCALSDIGIT_H
#define ALIFOCALSDIGIT_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */
//-----------------------------------------------------//
//                                                     //
//                                                     //
//  Date   : August 03 2009                            //
//  used to store the info into TreeS                  //
//                                                     //
//-----------------------------------------------------//
// Author - T. Gunji
//
#include "TObject.h"
class TClonesArray;

class AliFOCALsdigit : public TObject
{

 public:
  AliFOCALsdigit();
  AliFOCALsdigit(Int_t sec, Int_t seg, Int_t lay, 
		 Int_t row, Int_t col, Float_t edep, Float_t time);
  AliFOCALsdigit(AliFOCALsdigit *focalsdigit);
  AliFOCALsdigit (const AliFOCALsdigit &focalsdigit);  // copy constructor
  AliFOCALsdigit &operator=(const AliFOCALsdigit &focalsdigit); // assignment op

  virtual ~AliFOCALsdigit();

  Int_t   GetSector() const;
  Int_t   GetSegment() const;
  Int_t   GetLayer() const;
  Int_t   GetRow() const;
  Int_t   GetColumn() const;
  Float_t GetCellEdep() const;
  Float_t GetCellTime() const;

  
 protected:
  Int_t   fSec;        // Detector Number
  Int_t   fSeg;        // Serial Module Number
  Int_t   fLay; 
  Int_t   fRow;        // Row of wafer location in the brick 
  Int_t   fColumn;     // Column of wafer location in the brick 
  Float_t fEdep;       // Energy deposition in cell (summed up energy in layers in one segment)
  Float_t fTime;

  ClassDef(AliFOCALsdigit,5) // SDigits object for Detector set:FOCAL
};

#endif
