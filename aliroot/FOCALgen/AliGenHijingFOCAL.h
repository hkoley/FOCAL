#ifndef AliGenHijingFOCALFOCAL_H
#define AliGenHijingFOCALFOCAL_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */

/* $Id$ */

// Generator using HIJING as an external generator
// The main HIJING options are accessable for the user through this interface.
// andreas.morsch@cern.ch

#include "AliGenHijing.h"
#include "AliGenHijingEventHeader.h"
#include <TString.h>

class THijing;
class TParticle;
class TClonesArray;
class TGraph;


class AliGenHijingFOCAL : public AliGenHijing
{
  enum {kNoTrigger, kHardProcesses, kDirectPhotons, kDecayPhotons};
 public:
  virtual void Init();
  virtual Bool_t  CheckTrigger();
  ClassDef(AliGenHijingFOCAL, 9) // AliGenerator interface to Hijing
};
#endif
