/***************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

//
///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//  ALICE Forward Calorimeter   by T. Gunji                                  //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
////

#include <Riostream.h>
#include <TGeoGlobalMagField.h>
#include <TVirtualMC.h>
#include <TParticle.h>
#include <TGeoBBox.h>
#include <TGeoCompositeShape.h>

#include "AliConst.h"
#include "AliMagF.h"
#include "AliFOCALv2.h"
#include "AliFOCALGeometry.h"
#include "AliFOCALComposition.h"
#include "AliRun.h"
#include "AliMC.h"
#include "AliLog.h"
#include <TFile.h>

ClassImp(AliFOCALv2);

using namespace std;

//_____________________________________________________________________________
AliFOCALv2::AliFOCALv2() : fGeometryObject(0),
                           fGeometryFile(),
                           fMedSens(0),
                           fMedSensHCal(0),
                           fStoreFullShower(0),
                           fCurrentTrack(-1),
                           fCurrentMother(-1)
{
    //
    // Default constructor
    //
}

//_____________________________________________________________________________
AliFOCALv2::AliFOCALv2(const char *name, const char *title) : AliFOCAL(name, title),
                                                              fGeometryObject(0),
                                                              fGeometryFile("default"),
                                                              fMedSens(0),
                                                              fMedSensHCal(0),
                                                              fStoreFullShower(0),
                                                              fCurrentTrack(-1),
                                                              fCurrentMother(-1)
{
    //
    // Standard constructor
    //
    cout << " AliFOCALv2::AliFOCALv2 " << endl;
    AliLog::Message(AliLog::kInfo, Form("FoCAL geometry is configured by %s", fGeometryFile.Data()),
                    "AliFOCALv2.cxx", "AliFOCALv2.cxx", "AliFOCALv2(char *name, char *title)", "AliFOCALv2.cxx", __LINE__);

    fGeom = GetGeometry(fGeometryFile);
}

AliFOCALv2::AliFOCALv2(const char *name, const char *title, const char *gfile) : AliFOCAL(name, title),
                                                                                 fGeometryObject(0),
                                                                                 fGeometryFile(gfile),
                                                                                 fMedSens(0),
                                                                                 fMedSensHCal(0),
                                                                                 fStoreFullShower(0),
                                                                                 fCurrentTrack(-1),
                                                                                 fCurrentMother(-1)
{
    //
    // Standard constructor
    //
    cout << " AliFOCALv2::AliFOCALv2 " << endl;
    AliLog::Message(AliLog::kInfo, Form("FoCAL geometry is configured by %s", fGeometryFile.Data()),
                    "AliFOCALv2.cxx", "AliFOCALv2.cxx", "AliFOCALv2(char *name, char *title)", "AliFOCALv2.cxx", __LINE__);

    fGeom = GetGeometry(fGeometryFile);
}

AliFOCALv2 &AliFOCALv2::operator=(const AliFOCALv2 & /*ali*/)
{
    cout << "copy constructor" << endl;
    AliError("Assignment operator not allowed ");
    fGeom = GetGeometry();

    return *this;
}

//_____________________________________________________________________________
AliFOCALv2::~AliFOCALv2()
{
    //cout<<"destructor v2 "<<endl;
    //delete geom;
    // MvL: GeometryObject is owned by geometry class ?
    if (fGeometryObject)
    {
        fGeometryObject->Clear();
        fGeometryObject->Delete();
        fGeometryObject = 0;
    }
    //cout<<"destructor v2 end "<<endl;
}
//_____________________________________________________________________________
void AliFOCALv2::CreateGeometry()
{

    //// new geometry genetation
    //// AliFOCALGeometry has TObjArray of AliFOCALComposition
    //// This AliFOCALComposition knows
    ///// 1. What is the material?
    ////  2. Layer
    ////  3. Stack
    ////  4. center x  (in local frame of layer and wafer)
    ////  5. center y  (in local frame of layer and wafer)
    ////  6. center z  (in local frame of layer and wafer)
    ////  7. size of x, y, z

    /// -1 means get all the material object
    fGeometryObject = (TObjArray *)fGeom->GetFOCALMicroModule(-1);
    if (fGeometryObject == NULL)
    {
        cout << " fGeometryCompositionObject not found!!" << endl;
        return;
    }

    Int_t *idtmed = fIdtmed->GetArray() - 3599; //599 -> 3599

    Float_t pars[4];
    pars[0] = (fGeom->GetFOCALSizeX() + 2*fGeom->GetMiddleTowerOffset()) / 2;
    pars[1] = fGeom->GetFOCALSizeY() / 2;
    pars[2] = fGeom->GetFOCALSizeZ() / 2;  
    // Add space to place 2 SiPad layers in front of ECAL
    // The global position of ECAL and HCAL remains the same, but the FOCAL box needs to be slightly larger to accomodate
    //   the 2 SiPad layers which will sit at z=698 and 699cm (2 and 1 cm in front of ECAL)
    if (fGeom->GetInsertFrontPadLayers()) {
      pars[2] += 1.0;
    }
    pars[3] = 0;

    cout << "Creating FOCAL with dimensions X: " << (fGeom->GetFOCALSizeX() + 2*fGeom->GetMiddleTowerOffset()) << ", Y: " 
         << fGeom->GetFOCALSizeY() << ", Z: " << fGeom->GetFOCALSizeZ() + (fGeom->GetInsertFrontPadLayers() ? 2.0 : 0.0) << endl;

    gMC->Gsvolu("FOCAL", "BOX", idtmed[3698], pars, 4);

    CreateECAL();
    if (fGeom->GetUseHCALSandwich())
    {
        CreateHCALSandwich();
    }
    else
    {
        CreateHCALSpaghetti();
    }

    //gGeoManager->GetVolume("FOCAL")->SetVisLeaves();
    //gGeoManager->GetVolume("FOCAL")->SetVisibility();
    //gGeoManager->GetVolume("FOCAL")->SetVisContainers();
    gMC->Gspos("FOCAL", 1, "ALIC", 0, 0, fGeom->GetFOCALZ0()- (fGeom->GetInsertFrontPadLayers() ? 2.0 : 0.0), 0, "ONLY");
}

//____________________________________________________________________________
void AliFOCALv2::AddAlignableVolumes() const
{
    // Create entries for alignable volumes associating the symbolic volume
    // name with the corresponding volume path. Needs to be syncronized with
    // eventual changes in the geometry
    // Alignable volumes are:

    AddAlignableVolumesECAL();
    AddAlignableVolumesHCAL();
}
//____________________________________________________________________________
void AliFOCALv2::AddAlignableVolumesECAL() const
{
    TString vpsector = "ALIC_1/FOCAL_1/ECAL_1";
    TString snsector = "FOCAL/ECAL";

    if (!gGeoManager->SetAlignableEntry(snsector.Data(), vpsector.Data()))
    {
        cout << vpsector << endl;
        cout << snsector << endl;
        AliFatal("Unable to set alignable entry!");
    }
}
//____________________________________________________________________________
void AliFOCALv2::AddAlignableVolumesHCAL() const
{
    TString vpsector = "ALIC_1/FOCAL_1/HCAL_1";
    TString snsector = "FOCAL/HCAL";

    if (!gGeoManager->SetAlignableEntry(snsector.Data(), vpsector.Data()))
    {
        cout << snsector << endl;
        AliFatal("Unable to set alignable entry!");
    }
}

//_____________________________________________________________________________
void AliFOCALv2::CreateECAL()
{
    Int_t *idtmed = fIdtmed->GetArray() - 3599; //599 -> 3599

    ////// strategy to create the supermodule (tower)
    ////// 1. create tower correspinding to 5 PAD wafer
    ////// 2. create tower with PIX layers (NX:NY)

    /// make big volume containing all the longitudinal layers
    Float_t pars[4]; // this is EMSC Assembly
    pars[0] = fGeom->GetTowerSizeX() / 2 + fGeom->GetTowerGapSizeX() / 2;
    pars[1] = fGeom->GetTowerSizeY() / 2 + fGeom->GetTowerGapSizeY() / 2;
    //pars[2] = fGeom->GetFOCALSizeZ() / 2;
    pars[2] = fGeom->GetECALSizeZ() / 2;
    pars[3] = 0;
	//this shifts all the pixel layers to the center near the beampipe
	Double_t pixshift = fGeom->GetTowerSizeX()-(fGeom->GetGlobalPixelWaferSizeX()*fGeom->GetNumberOfPIXsInX()) ;

    float offset = pars[2];
    gMC->Gsvolu("EMSC1", "BOX", idtmed[3698], pars, 4);//Left towers (pixels shifted right)
    gMC->Gsvolu("EMSC2", "BOX", idtmed[3698], pars, 4);//Right towers (pixels shifted left)

    AliFOCALComposition *fComp = new AliFOCALComposition();
    for (int i = 0; i < fGeometryObject->GetEntriesFast(); i++)
    {
        fComp = (AliFOCALComposition *)fGeometryObject->UncheckedAt(i);

        pars[0] = fComp->SizeX() / 2;
        pars[1] = fComp->SizeY() / 2;
        pars[2] = fComp->SizeZ() / 2;
        pars[3] = 0;

        if (fComp->Material() == "PureW")
        {
            gMC->Gsvolu("EW1", "BOX", idtmed[3599], pars, 4);
            gGeoManager->GetVolume("EW1")->SetLineColor(kBlue);
            gMC->Gspos("EW1", i + 1, "EMSC1",
                       fComp->CenterX(), fComp->CenterY(), fComp->CenterZ() - offset, 0, "ONLY");
			gMC->Gspos("EW1", i + 1, "EMSC2",
					   fComp->CenterX(), fComp->CenterY(), fComp->CenterZ() - offset, 0, "ONLY");
        }
        if (fComp->Material() == "Alloy")
        {
            gMC->Gsvolu("EW1", "BOX", idtmed[3604], pars, 4);
            gMC->Gspos("EW1", i + 1, "EMSC1",
                       fComp->CenterX(), fComp->CenterY(), fComp->CenterZ() - offset, 0, "ONLY");
           	gMC->Gspos("EW1", i + 1, "EMSC2",
					   fComp->CenterX(), fComp->CenterY(), fComp->CenterZ() - offset, 0, "ONLY");
        }

        if (fComp->Material() == "G10")
        {
            gMC->Gsvolu("G10RO1", "BOX", idtmed[3601], pars, 4);
            gGeoManager->GetVolume("G10RO1")->SetLineColor(kGreen);
            gMC->Gspos("G10RO1", i + 1, "EMSC1",
                       fComp->CenterX(), fComp->CenterY(), fComp->CenterZ() - offset, 0, "ONLY");
			gMC->Gspos("G10RO1", i + 1, "EMSC2",
                       fComp->CenterX(), fComp->CenterY(), fComp->CenterZ() - offset, 0, "ONLY");
        }

        if (fComp->Material() == "Cu")
        {
            gMC->Gsvolu("EWCU", "BOX", idtmed[3602], pars, 4);
            gGeoManager->GetVolume("EWCU")->SetLineColor(kViolet);
            gMC->Gspos("EWCU", i + 1, "EMSC1",
                       fComp->CenterX(), fComp->CenterY(), fComp->CenterZ() - offset, 0, "ONLY");
            gMC->Gspos("EWCU", i + 1, "EMSC2",
                       fComp->CenterX(), fComp->CenterY(), fComp->CenterZ() - offset, 0, "ONLY");
        }

        if (fComp->Material() == "Air")
        {
            gMC->Gsvolu("EWAIR1", "BOX", idtmed[3698], pars, 4);
            gGeoManager->GetVolume("EWAIR1")->SetLineColor(kGray);
            gMC->Gspos("EWAIR1", i + 1, "EMSC1",
                       fComp->CenterX(), fComp->CenterY(), fComp->CenterZ() - offset, 0, "ONLY");
			gMC->Gspos("EWAIR1", i + 1, "EMSC2",
                       fComp->CenterX(), fComp->CenterY(), fComp->CenterZ() - offset, 0, "ONLY");
        }

        if (fComp->Material() == "Ceramic")
        {
            gMC->Gsvolu("EWAIR1", "BOX", idtmed[3607], pars, 4);
            gMC->Gspos("EWAIR1", i + 1, "EMSC1",
                       fComp->CenterX(), fComp->CenterY(), fComp->CenterZ() - offset, 0, "ONLY");
           	gMC->Gspos("EWAIR1", i + 1, "EMSC2",
                       fComp->CenterX(), fComp->CenterY(), fComp->CenterZ() - offset, 0, "ONLY");
        }

        if (fComp->Material() == "SiPad")
        {
            gMC->Gsvolu("EWSIPAD1", "BOX", idtmed[3600], pars, 4);
            gGeoManager->GetVolume("EWSIPAD1")->SetLineColor(kOrange - 7);
            int number = (fComp->Id()) + (fComp->Stack() << 12) + (fComp->Layer() << 16);
			//cout<<" pad : "<< fComp->Material()<<" "<<number<<" x: "<< pars[0] << " y: " << pars[1] <<" Z coord: " << fComp->CenterZ()-offset <<endl;
            gMC->Gspos("EWSIPAD1", number + 1, "EMSC1",
                       fComp->CenterX(), fComp->CenterY(), fComp->CenterZ() - offset, 0, "ONLY");
            gMC->Gspos("EWSIPAD1", number + 1, "EMSC2",
                       fComp->CenterX(), fComp->CenterY(), fComp->CenterZ() - offset, 0, "ONLY");
        }

        // Pixels (sensitive layer)
        if (fComp->Material() == "SiPix")
        {
            gMC->Gsvolu("EWSIPIX1", "BOX", idtmed[3600], pars, 4);
            gGeoManager->GetVolume("EWSIPIX1")->SetLineColor(kPink);

            int number = (fComp->Id()) + (fComp->Stack() << 12) + (fComp->Layer() << 16);
            gMC->Gspos("EWSIPIX1", number + 1, "EMSC1",
			           fComp->CenterX()-fGeom->GetGlobalPixelOffsetX()+pixshift, fComp->CenterY(), fComp->CenterZ() - offset, 0, "ONLY");
		    gMC->Gspos("EWSIPIX1", number + 1, "EMSC2",
		               fComp->CenterX()+fGeom->GetGlobalPixelOffsetX(), fComp->CenterY(), fComp->CenterZ() - offset, 0, "ONLY");
        }

        // Passive silicon
        if (fComp->Material() == "Si")
        {
            gMC->Gsvolu("EWSI1", "BOX", idtmed[3610], pars, 4);
            gGeoManager->GetVolume("EWSI1")->SetLineColor(kPink);
            gMC->Gspos("EWSI1", i + 1, "EMSC1",
                       fComp->CenterX(), fComp->CenterY(), fComp->CenterZ() - offset, 0, "ONLY");
		    gMC->Gspos("EWSI1", i + 1, "EMSC2",
		               fComp->CenterX(), fComp->CenterY(), fComp->CenterZ() - offset, 0, "ONLY");
        }
    }

    // Add the coldplates to each of the left and right towers
    TGeoBBox *coldPlateBox = new TGeoBBox("ColdPlateBox", fGeom->GetTowerSizeX()/2.0, fGeom->GetTowerGapSizeY()/2.0, fGeom->GetECALSizeZ()/2.0);     
    TGeoVolume *volumeColdPlate = 0x0;

    if (fGeom->GetTowerGapMaterial() == "Cu") {   // Copper
      volumeColdPlate = new TGeoVolume("volColdPlate", coldPlateBox, gGeoManager->GetMedium("FOCAL_Cu$"));
    }
    else if (fGeom->GetTowerGapMaterial() == "Al") {   // Aluminium
      volumeColdPlate = new TGeoVolume("volColdPlate", coldPlateBox, gGeoManager->GetMedium("FOCAL_AlPlate"));
    } else {
      volumeColdPlate = new TGeoVolume("volColdPlate", coldPlateBox, gGeoManager->GetMedium("FOCAL_AirGaps$"));  
    }
    volumeColdPlate->SetLineColor(kOrange);
    gMC->Gspos("volColdPlate", 1, "EMSC1", 0.0, fGeom->GetTowerSizeY()/2.0+fGeom->GetTowerGapSizeY()/2.0, 0.0, 0, "ONLY");
    gMC->Gspos("volColdPlate", 1, "EMSC2", 0.0, fGeom->GetTowerSizeY()/2.0+fGeom->GetTowerGapSizeY()/2.0, 0.0, 0, "ONLY");

    // Place the towers in the ECAL
    // --- Place the ECAL in FOCAL
    Double_t xp, yp, zp;
    Float_t fcal_pars[4];
    fcal_pars[0] = (fGeom->GetFOCALSizeX() + 2*fGeom->GetMiddleTowerOffset()) / 2;
    fcal_pars[1] = fGeom->GetFOCALSizeY() / 2;
    fcal_pars[2] = fGeom->GetECALSizeZ() / 2;
    fcal_pars[3] = 0;
	
    gMC->Gsvolu("ECAL", "BOX", idtmed[3698], fcal_pars, 4);

    // Create SiPad box for the two sensitive layers to be placed in front of ECAL
    TGeoBBox *siPadBox = new TGeoBBox("SiPadBox", fGeom->GetTowerSizeX() / 2 + fGeom->GetTowerGapSizeX() / 2, 
                                      fGeom->GetTowerSizeY() / 2 + fGeom->GetTowerGapSizeY() / 2, 0.03 / 2.0);
    TGeoVolume* volumeSiPad = new TGeoVolume("volSiPad", siPadBox, gGeoManager->GetMedium("FOCAL_SiSens$"));  
    volumeSiPad->SetLineColor(kOrange+7);

    for (int i = 0; i < fGeom->GetNumberOfTowersInX(); i++)
    {
        for (int j = 0; j < fGeom->GetNumberOfTowersInY(); j++)
        {
            int number = i + j * fGeom->GetNumberOfTowersInX();
            if (fGeom->GetGeoTowerCenter(number, xp, yp, zp) == true)
            {
                if(i == 0) {
                  gMC->Gspos("EMSC1", number + 1, "ECAL", xp, yp, 0, 0, "ONLY");
                  // Add the SiPad front volumes directly under the FOCAL volume
                  if (fGeom->GetInsertFrontPadLayers()) {
                    gMC->Gspos("volSiPad", -1*(number+1), "FOCAL", xp, yp, -1.0*fGeom->GetFOCALSizeZ() / 2.0, 0, "ONLY");
                    gMC->Gspos("volSiPad", -1*(fGeom->GetNumberOfTowersInX()*fGeom->GetNumberOfTowersInY() + number + 1), "FOCAL", xp-0.5, yp+0.5, -1.0*fGeom->GetFOCALSizeZ() / 2.0 + 1.0, 0, "ONLY");
                  }
                }
                if(i == 1) {
                  gMC->Gspos("EMSC2", number + 1, "ECAL", xp, yp, 0, 0, "ONLY");
                  // Add the SiPad front volumes directly under the FOCAL volume
                  if (fGeom->GetInsertFrontPadLayers()) {
                    gMC->Gspos("volSiPad", -1*(number+1), "FOCAL", xp, yp, -1.0*fGeom->GetFOCALSizeZ() / 2.0, 0, "ONLY");
                    gMC->Gspos("volSiPad", -1*(fGeom->GetNumberOfTowersInX()*fGeom->GetNumberOfTowersInY() + number + 1), "FOCAL", xp+0.5, yp+0.5, -1.0*fGeom->GetFOCALSizeZ() / 2.0 + 1.0, 0, "ONLY");
                  }
                }
            }
        }
    }

    gMC->Gspos("ECAL", 1, "FOCAL", 0, 0, fGeom->GetECALCenterZ() - fGeom->GetFOCALSizeZ() / 2.0 + (fGeom->GetInsertFrontPadLayers() ? 2.0 : 0.0), 0, "ONLY");
}
//_____________________________________________________________________________
void AliFOCALv2::CreateHCALSpaghetti()
{
    TGeoVolumeAssembly* volHCAL = new TGeoVolumeAssembly("HCAL");

    TGeoVolumeAssembly *HcalTube = gGeoManager->MakeVolumeAssembly("ScintCuTubes");

    TGeoVolume *volCuTube;
    TGeoVolume *volSciFi;

    Float_t RScint = 0.;
    Float_t Rin = 0.;
    Float_t Rout = 0.;
    Float_t Length = 0.;

    AliFOCALComposition *fComp = new AliFOCALComposition();
    for (int i = 0; i < fGeometryObject->GetEntriesFast(); i++)
    {
        fComp = (AliFOCALComposition *)fGeometryObject->UncheckedAt(i);

       Length = fComp->SizeZ() / 2;

        if (fComp->Material() == "Pb")
        {
            Rout = fComp->SizeX() / 2;
            TGeoMedium* medium = gGeoManager->GetMedium("FOCAL_Pb$");
            volCuTube = gGeoManager->MakeTube("Tube", medium, Rin, Rout, Length);
            volCuTube->SetLineWidth(2);
            volCuTube->SetLineColor(kRed);
            HcalTube->AddNode(volCuTube, 1, 0x0);
        }
        if (fComp->Material() == "Scint")
        {
            RScint = fComp->SizeX() / 2;
            Rin = RScint + 0.005;
            TGeoMedium* medium = gGeoManager->GetMedium("FOCAL_Scint$");
            volSciFi = gGeoManager->MakeTube("ScintFiber", medium, 0., RScint, Length);
            volSciFi->SetLineWidth(2);
            volSciFi->SetLineColor(kBlue);
            HcalTube->AddNode(volSciFi, 1, 0x0);
        }
        if (fComp->Material() == "CuHCAL")
        {
            Rout = fComp->SizeX() / 2;
            TGeoMedium* medium = gGeoManager->GetMedium("FOCAL_Cu$");
            volCuTube = gGeoManager->MakeTube("Tube", medium, Rin, Rout, Length);
            volCuTube->SetLineWidth(2);
            volCuTube->SetLineColor(kRed);
            HcalTube->AddNode(volCuTube, 1, 0x0);
        }
    }

    double TowerSize = fGeom->GetHCALTowerSize();
    double CuBoxThickness = 0.3; // Thickness of the Cu box carrying capillary tubes

    TGeoBBox *ODBox = new TGeoBBox("TowerOD", TowerSize/2, TowerSize/2, Length);
    TGeoBBox *IDBox = new TGeoBBox("TowerID", (TowerSize - CuBoxThickness)/2, (TowerSize - CuBoxThickness)/2, Length + 0.01);
    TGeoCompositeShape *TowerHCAL = new TGeoCompositeShape("TowerHCAL", "TowerOD - TowerID");
    TGeoVolume *volTower = new TGeoVolume("volTower", TowerHCAL, gGeoManager->GetMedium("FOCAL_Cu$"));
    volTower->SetLineWidth(2);
    volTower->SetLineColor(42);

    TGeoVolumeAssembly* volTowerHCAL = new TGeoVolumeAssembly("volTowerHCAL");
    volTowerHCAL->AddNode(volTower, 1, 0x0);

    int Rows = 0;
    Float_t RowPos = 0.;
    Int_t Columns = 0;
    Int_t NumTubes = 1;
    // Packing circles in Hexagonal shape
    while (RowPos + CuBoxThickness/2 + Rout + 2*Rout < TowerSize)
    {
        Columns = 0;
        Float_t ColumnPos = (Rows % 2 == 0) ? 0. : Rout;
        while (ColumnPos + CuBoxThickness/2 + Rout + 2*Rout < TowerSize)
        {

            TGeoTranslation *trans = new TGeoTranslation(ColumnPos - TowerSize / 2 + CuBoxThickness/2 + Rout, RowPos - TowerSize / 2 + CuBoxThickness/2 + Rout, 0.);

            trans->SetName(Form("trans_Num_%d", NumTubes));
            trans->RegisterYourself();

            volTowerHCAL->AddNode(HcalTube, NumTubes, trans);
            // volTowerHCAL->AddNode(volCuTube, NumTubes, trans);
            // volTowerHCAL->AddNode(volSciFi, NumTubes, trans);

            Columns++;
            ColumnPos = Columns * 2 * Rout + ((Rows % 2 == 0) ? 0. : Rout);
            NumTubes++;
        }

        Rows++;
        RowPos = Rows * 2 * Rout * TMath::Sin(TMath::Pi() / 3);
    }

    // Define the distance from the beam pipe in which towers will ommitted
    Double_t BeamPipeRadius = 3.6;                             // in cm
    Double_t TowerHalfDiag = TMath::Sqrt2() * 0.5 * TowerSize; // tower half diagonal
    Double_t MinRadius = BeamPipeRadius + TowerHalfDiag;

    float SizeXHCAL = fGeom->GetHCALTowersInX() * TowerSize;
    float SizeYHCAL = fGeom->GetHCALTowersInY() * TowerSize;

    int nTowersX = fGeom->GetHCALTowersInX();
    int nTowersY = fGeom->GetHCALTowersInY();

    Rows = 0;
    Columns = 0;
    RowPos = 0.;
    Int_t NumTowers = 1;
    for (Rows = 0; Rows < nTowersY; Rows++)
    {

        Float_t ColumnPos = 0.;
        RowPos = Rows * TowerSize;
        for (Columns = 0; Columns < nTowersX; Columns++)
        {
            ColumnPos = Columns * TowerSize;
            TGeoTranslation *trans = new TGeoTranslation(ColumnPos - SizeXHCAL / 2 + TowerSize / 2, RowPos - SizeYHCAL / 2 + TowerSize / 2, 0.);

            if(trans->GetTranslation()[1] < MinRadius && trans->GetTranslation()[1] > - MinRadius){
                trans->SetDx(int(trans->GetTranslation()[0]) <= 0 ? ColumnPos - SizeXHCAL / 2 + TowerSize / 2 - MinRadius : ColumnPos - SizeXHCAL / 2 + TowerSize / 2 + MinRadius - TowerSize);
            }

            // Remove the Towers that overlaps with the beam pipe
            Double_t RadialDistance = TMath::Power(trans->GetTranslation()[0], 2) + TMath::Power(trans->GetTranslation()[1], 2);

            if (RadialDistance < MinRadius * MinRadius || TMath::Abs(trans->GetTranslation()[0]) > SizeXHCAL/2)
            {
                continue;
            }

            //Adding the Tower to the HCAL
            volHCAL->AddNode(volTowerHCAL, NumTowers, trans);

            NumTowers++;
        }
    }

    cout << "Number of Towers is: " << (NumTowers - 1) << endl;
    cout << "Number of tubes is: " << (NumTubes - 1) * (NumTowers - 1) << endl;

    gMC->Gspos("HCAL", 1, "FOCAL", 0, 0, fGeom->GetHCALCenterZ() - fGeom->GetFOCALSizeZ() / 2 + 0.01 + (fGeom->GetInsertFrontPadLayers() ? 2.0 : 0.0), 0, "ONLY");
}
//_____________________________________________________________________________
void AliFOCALv2::CreateHCALSandwich()
{
    TGeoVolumeAssembly* volHCAL = new TGeoVolumeAssembly("HCAL");

    /// make big volume containing all the longitudinal layers
    Float_t pars[4]; // this is HMSC Assembly
    pars[0] = fGeom->GetHCALTowerSize() / 2;
    pars[1] = fGeom->GetHCALTowerSize() / 2;
    pars[2] = fGeom->GetECALSizeZ() + fGeom->GetHCALSizeZ() / 2; //ECAL sizeZ is already added to the HCAL materials CenterZ, so it is also treated as offset
    pars[3] = 0;

    float offset = pars[2];

    TGeoVolumeAssembly* volTower = new TGeoVolumeAssembly("Tower");

    AliFOCALComposition *fComp = new AliFOCALComposition();

    int iCu(0), iScint(0);
    for (int i = 0; i < fGeometryObject->GetEntriesFast(); i++)
    {
        fComp = (AliFOCALComposition *)fGeometryObject->UncheckedAt(i);

        pars[0] = fComp->SizeX() / 2;
        pars[1] = fComp->SizeY() / 2;
        pars[2] = fComp->SizeZ() / 2;
        pars[3] = 0;

        // HCal materials

        if (fComp->Material() == "Pb")
        {
            iCu++;
            const TGeoMedium* medium = gGeoManager->GetMedium("FOCAL_Pb$");
            const TGeoBBox* HPadBox = new TGeoBBox("HPadBox",pars[0], pars[1], pars[2]);
            TGeoVolume* HPad = new TGeoVolume("HPad", HPadBox, medium);
            HPad->SetLineColor(kRed);
            TGeoTranslation* trans = new TGeoTranslation(fComp->CenterX(), fComp->CenterY(), fComp->CenterZ() - offset);
            volTower->AddNode(HPad, iCu, trans);
        }
        if (fComp->Material() == "Scint")
        {
            iScint++;
            const TGeoMedium* medium = gGeoManager->GetMedium("FOCAL_Scint$");
            const TGeoBBox* HScintBox = new TGeoBBox("HScintBox",pars[0], pars[1], pars[2]);
            TGeoVolume* HScint = new TGeoVolume("HScint", HScintBox, medium);
            HScint->SetLineColor(kBlue);
            TGeoTranslation* trans = new TGeoTranslation(fComp->CenterX(), fComp->CenterY(), fComp->CenterZ() - offset);
            volTower->AddNode(HScint, iScint, trans);
        }
        if (fComp->Material() == "CuHCAL")
        {
            iCu++;
            const TGeoMedium* medium = gGeoManager->GetMedium("FOCAL_Cu$");
            const TGeoBBox* HPadBox = new TGeoBBox("HPadBox",pars[0], pars[1], pars[2]);
            TGeoVolume* HPad = new TGeoVolume("HPad", HPadBox, medium);
            HPad->SetLineColor(kRed);
            TGeoTranslation* trans = new TGeoTranslation(fComp->CenterX(), fComp->CenterY(), fComp->CenterZ() - offset);
            volTower->AddNode(HPad, iCu, trans);
        }
    }
    Double_t TowerSize = fGeom->GetHCALTowerSize();

    // Define the distance from the beam pipe in which towers will ommitted
    Double_t BeamPipeRadius = 3.6;                             // in cm
    Double_t TowerHalfDiag = TMath::Sqrt2() * 0.5 * TowerSize; // tower half diagonal
    Double_t MinRadius = BeamPipeRadius + TowerHalfDiag;

    float SizeXHCAL = fGeom->GetHCALTowersInX() * TowerSize;
    float SizeYHCAL = fGeom->GetHCALTowersInY() * TowerSize;

    int nTowersX = fGeom->GetHCALTowersInX();
    int nTowersY = fGeom->GetHCALTowersInY();

    Int_t Rows = 0;
    Int_t Columns = 0;
    Double_t RowPos = 0.;
    Int_t NumTowers = 1;

    // Arranging towers
    for (Rows = 0; Rows < nTowersY; Rows++)
    {
        Columns = 0;
        Float_t ColumnPos = 0.;
        RowPos = Rows * TowerSize;
        for (Columns = 0; Columns < nTowersX; Columns++)
        {
            ColumnPos = Columns * TowerSize;

            TGeoTranslation *trans = new TGeoTranslation(ColumnPos - SizeXHCAL / 2 + TowerSize / 2, RowPos - SizeYHCAL / 2 + TowerSize / 2, 0.);

            // Remove the Towers that overlaps with the beam pipe
            Double_t RadialDistance = TMath::Power(ColumnPos - SizeXHCAL / 2 + TowerSize / 2, 2) + TMath::Power(RowPos - SizeYHCAL / 2 + TowerSize / 2, 2);

            if (RadialDistance < MinRadius * MinRadius)
            {
                continue;
            }

            //Adding the Tower to the HCAL
            volHCAL->AddNode(volTower, NumTowers, trans);

            NumTowers++;
        }
    }
    cout << "Number of Towers is: " << (NumTowers - 1) << endl;

    TGeoVolume* volFOCAL = gGeoManager->GetVolume("FOCAL");
    volFOCAL->AddNode(volHCAL, 1, new TGeoTranslation(0, 0, fGeom->GetHCALCenterZ() - fGeom->GetFOCALSizeZ() / 2 + 0.01 + (fGeom->GetInsertFrontPadLayers() ? 2.0 : 0.0)));//0.01 to avoid overlap with ECAL
}

//_____________________________________________________________________________
void AliFOCALv2::DrawModule() const
{
    //
    // Draw a shaded view of the Photon Multiplicity Detector
    //
}

//_____________________________________________________________________________
void AliFOCALv2::CreateMaterials()
{
    //
    // Create materials for the FOCAL
    //
    // ORIGIN    : T. Gunuji
    //

    Int_t isxfld = ((AliMagF *)TGeoGlobalMagField::Instance()->GetField())->Integ();
    Float_t sxmgmx = ((AliMagF *)TGeoGlobalMagField::Instance()->GetField())->Max();

    // --- Define the various materials for GEANT ---

    //// W Tungsten
    Float_t aW = 183.84;
    Float_t zW = 74.0;
    Float_t dW = 19.3;
    Float_t x0W = 0.35;
    AliMaterial(0, "W $", aW, zW, dW, x0W, 17.1);

    /// Silicon
    Float_t aSi = 28.09;
    Float_t zSi = 14.0;
    Float_t dSi = 2.33;
    Float_t x0Si = 9.36;
    AliMaterial(1, "Si $", aSi, zSi, dSi, x0Si, 18.5);

    // G10
    Float_t aG10[4] = {1., 12.011, 15.9994, 28.086};
    Float_t zG10[4] = {1., 6., 8., 14.};
    //PH  Float_t wG10[4]={0.148648649,0.104054054,0.483499056,0.241666667};
    Float_t wG10[4] = {0.15201, 0.10641, 0.49444, 0.24714};
    AliMixture(2, "G10  $", aG10, zG10, 1.7, 4, wG10);

    // Cu
    AliMaterial(3, "Cu   $", 63.54, 29., 8.96, 1.43, 15.);

    // Steel
    Float_t aSteel[4] = {55.847, 51.9961, 58.6934, 28.0855};
    Float_t zSteel[4] = {26., 24., 28., 14.};
    Float_t wSteel[4] = {.715, .18, .1, .005};
    Float_t dSteel = 7.88;
    AliMixture(4, "STAINLESS STEEL$", aSteel, zSteel, dSteel, 4, wSteel);

    //// 94W-4Ni-2Cu
    Float_t aAlloy[3] = {183.84, 58.6934, 63.54};
    Float_t zAlloy[3] = {74.0, 28, 29};
    Float_t wAlloy[3] = {0.94, 0.04, 0.02};
    Float_t dAlloy = wAlloy[0] * 19.3 + wAlloy[1] * 8.908 + wAlloy[2] * 8.96;
    AliMixture(5, "Alloy $", aAlloy, zAlloy, dAlloy, 3, wAlloy);

    //Ceramic
    // Ceramic   97.2% Al2O3 , 2.8% SiO2
    // Float_t wcer[2]={0.972,0.028};  // Not used
    Float_t aal2o3[2] = {26.981539, 15.9994};
    Float_t zal2o3[2] = {13., 8.};
    Float_t wal2o3[2] = {2., 3.};
    Float_t denscer = 3.6;
    // SiO2
    Float_t aglass[2] = {28.0855, 15.9994};
    Float_t zglass[2] = {14., 8.};
    Float_t wglass[2] = {1., 2.};
    Float_t dglass = 2.65;
    AliMixture(6, "Al2O3   $", aal2o3, zal2o3, denscer, -2, wal2o3);
    AliMixture(7, "glass   $", aglass, zglass, dglass, -2, wglass);

    // Ceramic is a mixtur of glass and Al2O3 ?
    //   Not clear how to do this with AliMixture
    //   Not needed; so skip for now
    // Use Al2O3 instead:
    AliMixture(8, "Ceramic    $", aal2o3, zal2o3, denscer, -2, wal2o3);

    // Aluminum
    AliMaterial(9, "Al",  26.98, 13.0, 2.7,    8.9,  37.2);

    //// Pb
    AliMaterial(10, "Pb    $", 207.19, 82., 11.35, .56, 18.5);

    //// Scintillator (copied from EMCal)
    // --- The polysterene scintillator (CH) ---
    Float_t aP[2] = {12.011, 1.00794};
    Float_t zP[2] = {6.0, 1.0};
    Float_t wP[2] = {1.0, 1.0};
    Float_t dP = 1.032;
    AliMixture(11, "Polystyrene$", aP, zP, dP, -2, wP);

    //Air
    Float_t aAir[4] = {12.0107, 14.0067, 15.9994, 39.948};
    Float_t zAir[4] = {6., 7., 8., 18.};
    Float_t wAir[4] = {0.000124, 0.755268, 0.231781, 0.012827};
    Float_t dAir1 = 1.20479E-10;
    Float_t dAir = 1.20479E-3;
    AliMixture(98, "Vacum$", aAir, zAir, dAir1, 4, wAir);
    AliMixture(99, "Air  $", aAir, zAir, dAir, 4, wAir);

    // Define tracking media
    // format
    Float_t tmaxfdSi = 10.0; //0.1; // .10000E+01; // Degree
    Float_t stemaxSi = 0.1;  //  .10000E+01; // cm
    Float_t deemaxSi = 0.1;  // 0.30000E-02; // Fraction of particle's energy 0<deemax<=1
    //Float_t epsilSi  = 1.e-3;//1e-3;//1.0E-4;// .10000E+01;
    Float_t epsilSi = 1.e-3; //1.0E-4;// .10000E+01; // This drives the step size ? 1e-4 makes multiple steps even in pixels?
    Float_t stminSi = 0.001; // cm "Default value used"

    Float_t epsil = 0.001;
    // MvL: need to look up itdmed dynamically?
    // or move to TGeo: uses pointers for medium

    /// W plate -> idtmed[3599];
    AliMedium(0, "W conv.$", 0, 0, isxfld, sxmgmx, 10.0, 0.1, 0.1, 0.1, 0.1, 0, 0);
    /// Si plate  -> idtmed[3600];
    AliMedium(1, "SiSens$", 1, 0, isxfld, sxmgmx, tmaxfdSi, stemaxSi, deemaxSi, epsilSi, stminSi, 0, 0);

    //// G10 plate -> idtmed[3601];
    AliMedium(2, "G10 plate$", 2, 0, isxfld, sxmgmx, 10.0, 0.1, 0.1, 0.1, 0.01, 0, 0);

    //// Cu plate --> idtmed[3602];
    AliMedium(3, "Cu$", 3, 0, isxfld, sxmgmx, 10.0, 0.1, 0.1, 0.1, 0.1, 0, 0);

    //// S steel -->  idtmed[3603];
    AliMedium(4, "S  steel$", 4, 0, isxfld, sxmgmx, 10.0, 0.1, 0.1, 0.1, 0.0001, 0, 0);

    //// Alloy --> idtmed[3604];
    AliMedium(5, "Alloy  conv.$", 5, 0, isxfld, sxmgmx, 10.0, 0.1, 0.1, 0.1, 0.1, 0, 0);

    //// Ceramic --> idtmed[3607]
    AliMedium(8, "Ceramic$", 8, 0, isxfld, sxmgmx, 10.0, 0.01, 0.1, 0.003, 0.003, 0, 0);

    // HCAL materials   // Need to double-check  tracking pars for this
    /// Pb plate --> idtmed[3608]
    AliMedium(9, "Pb$", 10, 0, isxfld, sxmgmx, 10.0, 0.1, 0.1, 0.1, 0.1, 0, 0);
    /// Scintillator --> idtmed[3609]
    AliMedium(10, "Scint$", 11, 0, isxfld, sxmgmx, 10.0, 0.1, 0.1, 0.001, 0.001, 0, 0);

    /// Si plate  -> idtmed[3610];
    AliMedium(11, "Si insens$", 1, 0, 10.0, 0.1, 0.1, epsil, 0.001, 0, 0);

    // Al for the cold plates 
    AliMedium(12, "AlPlate", 9, 0, isxfld, sxmgmx, 10.0, 0.1, 0.1, 0.1, 0.1, 0, 0);

    /// idtmed[3697]
    AliMedium(98, "Vacuum$", 98, 0, isxfld, sxmgmx, 10.0, 1.0, 0.1, 0.1, 1.0, 0, 0);

    /// idtmed[3698]
    AliMedium(99, "AirGaps$", 99, 0, isxfld, sxmgmx, 10.0, 1.0, 0.1, epsil, 0.001, 0, 0);
}

//_____________________________________________________________________________
void AliFOCALv2::Init()
{
    //
    // Initialises FOCAL detector after it has been built
    //
    Int_t i;
    //  kdet=1;
    //
    if (AliLog::GetGlobalDebugLevel() > 0)
    {
        printf("\n%s: ", ClassName());
        for (i = 0; i < 35; i++)
            printf("*");
        printf(" FOCAL_INIT ");
        for (i = 0; i < 35; i++)
            printf("*");
        printf("\n%s: ", ClassName());
        printf("FOCAL simulation package (v2) initialised\n");
        printf("%s: parameters of FOCAL\n", ClassName());
        //printf("%s: %10.2f %10.2f %10.2f %10.2f\n", ClassName(), fcl_Z0, fcl_CRDXY, fcl_DPS, fcl_DW);
        printf("%s: ", ClassName());
        for (i = 0; i < 80; i++)
            printf("*");
        printf("\n");
    }

    Int_t *idtmed = fIdtmed->GetArray() - 3599; // MvL: why do we have this offset?
    fMedSens = idtmed[3600];

    fMedSensHCal = idtmed[3609];
    // --- Generate explicitly delta rays in the W, Al and Si ---
    gMC->Gstpar(idtmed[3599], "LOSS", 3.);
    gMC->Gstpar(idtmed[3599], "DRAY", 1.);
    gMC->Gstpar(idtmed[3611], "LOSS", 3.);
    gMC->Gstpar(idtmed[3611], "DRAY", 1.);
    // --- Energy cut-offs in the Pb and Al to gain time in tracking ---
    // --- without affecting the hit patterns ---
    gMC->Gstpar(idtmed[3599], "CUTGAM", 0.5e-4);
    gMC->Gstpar(idtmed[3599], "CUTELE", 1e-4);
    gMC->Gstpar(idtmed[3599], "CUTNEU", 1e-4);
    gMC->Gstpar(idtmed[3599], "CUTHAD", 1e-4);
    gMC->Gstpar(idtmed[3611], "CUTGAM", 0.5e-4);
    gMC->Gstpar(idtmed[3611], "CUTELE", 1e-4);
    gMC->Gstpar(idtmed[3611], "CUTNEU", 1e-4);
    gMC->Gstpar(idtmed[3611], "CUTHAD", 1e-4);
    //  gMC->Gstpar(idtmed[3599], "BCUTE", 1e-5);
    //  gMC->Gstpar(idtmed[3599], "BCUTM", 1e-5);
    //  gMC->Gstpar(idtmed[3599], "DCUTE", 1e-5);
    //  gMC->Gstpar(idtmed[3599], "DCUTM", 1e-5);

    gMC->Gstpar(idtmed[3604], "LOSS", 3.);
    gMC->Gstpar(idtmed[3604], "DRAY", 1.);
    // --- Energy cut-offs in the Pb and Al to gain time in tracking ---
    // --- without affecting the hit patterns ---
    gMC->Gstpar(idtmed[3604], "CUTGAM", 0.5e-4);
    gMC->Gstpar(idtmed[3604], "CUTELE", 1e-4);
    gMC->Gstpar(idtmed[3604], "CUTNEU", 1e-4);
    gMC->Gstpar(idtmed[3604], "CUTHAD", 1e-4);
    //  gMC->Gstpar(idtmed[3599], "BCUTE", 1e-5);
    //  gMC->Gstpar(idtmed[3599], "BCUTM", 1e-5);
    //  gMC->Gstpar(idtmed[3599], "DCUTE", 1e-5);
    //  gMC->Gstpar(idtmed[3599], "DCUTM", 1e-5);

    gMC->Gstpar(idtmed[3600], "LOSS", 1.);
    gMC->Gstpar(idtmed[3600], "DRAY", 1.);

    gMC->Gstpar(idtmed[3601], "LOSS", 3.);
    gMC->Gstpar(idtmed[3601], "DRAY", 1.);

    gMC->Gstpar(idtmed[3698], "LOSS", 3.);
    gMC->Gstpar(idtmed[3698], "DRAY", 1.);

    gMC->Gstpar(idtmed[3601], "CUTGAM", 1e-4);
    gMC->Gstpar(idtmed[3601], "CUTELE", 1e-4);
    gMC->Gstpar(idtmed[3601], "CUTNEU", 1e-4);
    gMC->Gstpar(idtmed[3601], "CUTHAD", 1e-4);

    gMC->Gstpar(idtmed[3698], "CUTGAM", 1e-4);
    gMC->Gstpar(idtmed[3698], "CUTELE", 1e-4);
    gMC->Gstpar(idtmed[3698], "CUTNEU", 1e-4);
    gMC->Gstpar(idtmed[3698], "CUTHAD", 1e-4);

    gMC->Gstpar(idtmed[3600], "CUTGAM", 1e-5); //10keV
    gMC->Gstpar(idtmed[3600], "CUTELE", 1e-5); //10keV
    gMC->Gstpar(idtmed[3600], "CUTNEU", 1e-5);
    gMC->Gstpar(idtmed[3600], "CUTHAD", 1e-5);
    gMC->Gstpar(idtmed[3600], "CUTMUO", 1e-5);
    gMC->Gstpar(idtmed[3600], "BCUTE", 1e-5);
    gMC->Gstpar(idtmed[3600], "BCUTM", 1e-5);
    gMC->Gstpar(idtmed[3600], "DCUTE", 1e-5);
    gMC->Gstpar(idtmed[3600], "DCUTM", 1e-5);
}

//_____________________________________________________________________________
void AliFOCALv2::StepManager()
{
    //
    // Called at each step in the FOCAL
    //

    Int_t copy;
    Float_t hits[5] = {0.0,0.0,0.0,0.0,0.0};
    Float_t destep;
    Float_t center[3] = {0, 0, 0};
    Int_t vol[4] = {0,0,0,0};
    //char namep[100];

    AliDebug(2, Form("Current volume is %s", gMC->CurrentVolName()));

    if ((gMC->CurrentMedium() == fMedSens || gMC->CurrentMedium() == fMedSensHCal) && (destep = gMC->Edep()))
    {

        gMC->CurrentVolID(copy);
        vol[0] = copy; // this is current volume

        //sprintf(namep,"%s",gMC->CurrentVolName());
        //printf("Current vol is %s vol[0]=%d\n",namep, copy);

        gMC->CurrentVolOffID(1, copy);
        vol[1] = copy; // this is mother volume (segment 0-3)

        //sprintf(namep,"%s",gMC->CurrentVolOffName(1));
        //printf("Current vol 1 is %s vol[1]=%d\n",namep, copy);

        gMC->CurrentVolOffID(2, copy);
        vol[2] = copy; // this is mother volume (brik & sector 1-12)

        //sprintf(namep,"%s",gMC->CurrentVolOffName(2));
        //printf("Current vol 11 is %s vol[2]=%d\n",namep, copy);

        //    gMC->CurrentVolOffID(3,copy);
        //    sprintf(namep,"%s",gMC->CurrentVolOffName(3));
        //    printf("Current vol 111 is %s %d\n",namep, copy);

        //printf("volume number %d,%d,%d %f\n",vol[0],vol[1],vol[2],destep*1000000);

        vol[3] = -1;
        Double_t x, y, z;
		Double_t pixelwaferx = fGeom->GetGlobalPixelWaferSizeX();
		Double_t pixelwafery = fGeom->GetGlobalPixelWaferSizeX();
        Double_t pixelsize = fGeom->GetGlobalPixelSize();

        gMC->TrackPosition(x, y, z);
        AliDebug(2, Form("track %d position %g %g %g dE %f", gAlice->GetMCApp()->GetCurrentTrackNumber(), x, y, z, destep));
        gMC->Gdtom(center, hits, 1);

        if (gMC->CurrentMedium() == fMedSensHCal ||
            (gMC->CurrentMedium() == fMedSens && vol[0]<0))
        {
            // HCal; use position directly
            vol[3] = 0;
            hits[0] = x;
            hits[1] = y;
            hits[2] = z;
        }
        else
        {
            
            double x_loc = x - hits[0];
            double y_loc = y - hits[1];

            float pixel_nbr_x = ((x_loc + pixelwaferx*0.5) / (pixelsize));
            float pixel_nbr_y = ((y_loc + pixelwafery*0.5) / (pixelsize));

            int pixel_number_x;
            pixel_number_x = static_cast<int>(pixel_nbr_x);
            if (pixel_number_x < 0) // from debug printouts, looks like there is a rounding issue in x, atr the 0.0005 cm level
                pixel_number_x = 0;
            int pixel_number_y;
            pixel_number_y = static_cast<int>(pixel_nbr_y);
            if (pixel_number_y < 0) // probably not needed
                pixel_number_y = 0;

            vol[3] = (pixel_number_x << 16) | (pixel_number_y & 0xfff);

            //    cout << "PadLocation2: " << hits[0] << ", " << hits[1] << endl;

            Double_t px, py, pz;
            unsigned int pixel = static_cast<int>(vol[3]);
            int pixel_y = pixel & 0xfff;
            int pixel_x = (pixel >> 16) & 0xfff;

            double x1, y1;
            x1 = (pixel_x)*pixelsize + 0.5 * pixelsize - pixelwaferx*0.5;
            y1 = (pixel_y)*pixelsize + 0.5 * pixelsize - pixelwafery*0.5;

            px = x1 + hits[0];
            py = y1 + hits[1];
            pz = hits[2];

            hits[0] = px;
            hits[1] = py;
            hits[2] = pz;
        }

        hits[3] = destep * 1e9; //Number in eV  (GeV->eV)

        hits[4] = gMC->TrackTime();

        if (fStoreFullShower)
            AddHit(gAlice->GetMCApp()->GetCurrentTrackNumber(), vol, hits);
        else
        { // determine track number that entered FOCAL
            if (gAlice->GetMCApp()->GetCurrentTrackNumber() == fCurrentTrack)
            {
                if (fCurrentMother < 0)
                {
                    TParticle *part = gAlice->GetMCApp()->Particle(gAlice->GetMCApp()->GetCurrentTrackNumber());
                    AliWarning(Form("Did not find mother track for track %d, pid %d", gAlice->GetMCApp()->GetCurrentTrackNumber(), (part ? part->GetPdgCode() : 0)));
                }
                else
                    AddHit(fCurrentMother, vol, hits);
            }
            else
            {
                Int_t parent = gAlice->GetMCApp()->GetCurrentTrackNumber();
                Int_t trackNumber = -1;
                Int_t isin = 1;
                TParticle *part = 0;
                do
                {
                    trackNumber = parent;
                    part = gAlice->GetMCApp()->Particle(trackNumber);
                    parent = part->GetFirstMother();
                } while (parent != -1 && parent != fCurrentTrack &&
                         (isin = IsInFOCAL(part->Vx(), part->Vy(), part->Vz())));

                if (parent != -1 && (parent == fCurrentTrack)) // share parent; change fCurrentTrack
                    fCurrentTrack = gAlice->GetMCApp()->GetCurrentTrackNumber();
                else if (!(isin = IsInFOCAL(part->Vx(), part->Vy(), part->Vz())))
                { // Note: recalculating isin, because evaluation may be skipped in while loop of parent == -1
                    fCurrentTrack = gAlice->GetMCApp()->GetCurrentTrackNumber();
                    fCurrentMother = trackNumber;
                }
                else
                {
                    fCurrentTrack = gAlice->GetMCApp()->GetCurrentTrackNumber();
                    fCurrentMother = -1;
                    part = gAlice->GetMCApp()->Particle(gAlice->GetMCApp()->GetCurrentTrackNumber());
                    AliWarning(Form("Could not find mother track! track %d pdg %d parent %d isin %d vtx %f %f %f", gAlice->GetMCApp()->GetCurrentTrackNumber(), (part ? part->GetPdgCode() : 0), parent, isin, part->Vx(), part->Vy(), part->Vz()));
                }
                if (fCurrentMother >= 0)
                    AddHit(fCurrentMother, vol, hits);
            }
        }
    }
}

void AliFOCALv2::FinishPrimary()
{
    //
    // finish primary; reset indices for mother particles etc
    //
    fCurrentMother = -1;
    fCurrentTrack = -1;
}

//  LocalWords:  EMSC
