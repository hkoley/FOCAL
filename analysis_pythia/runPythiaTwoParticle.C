void runPythiaTwoParticle(Int_t startFolder, Int_t endFolder, Int_t startEvent, Int_t endEvent) {
  gROOT->LoadMacro("$HOME/users/mvl/FOCAL/FOCAL/LoadFOCAL.C");
  LoadFOCAL("/home/focal/users/mvl/FOCAL/FOCAL");

  gSystem->AddIncludePath("-I$ALICE_ROOT/include -I$HOME/users/mvl/FOCAL/FOCAL");
  gROOT->LoadMacro("pythiaTwoParticleAlgo.cxx+g");
  
  //char *dataSampleTag="pythia_MBtrig_13tev_loi2014";
  //char* simFolder="/home/focal/storage/sims/pythia/MBtrig_13tev";
  //char *dataSampleTag="pythia_MBEMtrig_13tev_loi2014";
  //char* simFolder="/home/focal/storage/sims/pythia/MBEMtrig_13tev";
  //char *dataSampleTag="pythia_MBEMdecaytrig_13tev_loi2014";
  //char* simFolder="/home/focal/storage/sims/pythia/MBEMdecaytrig_13tev";
  char *dataSampleTag="pythia_MBEMdecaytrig_13tev_new_loi2014";
  char* simFolder="/home/focal/storage/sims/pythia/MBEMdecaytrig_13tev_new";
  //char *dataSampleTag="pythia_dirgamma_13tev_loi2014";
  //char* simFolder="/home/focal/storage/sims/pythia/dirgamma_13tev";

  char *outputdir = "results";
  char *clustersFolder="../clustering/results";

  pythiaTwoParticleAlgo(startFolder, endFolder, startEvent, endEvent, simFolder, clustersFolder, dataSampleTag, outputdir);
}
