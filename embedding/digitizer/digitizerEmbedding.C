Int_t backgroundFolders = 40;
Int_t backgroundEventsPerFolder = 5;

Int_t randomizeBackground = 0; // if true, pick random bkg event; otherwise go algorithmically
    
char signalFolder[200] = "/home/staff/leeuw179/focal_sims/pi0_pt0-20_dpix30mum_wpix50mum";
char backgroundFolder[200] = "/home/staff/leeuw179/focal_sims/hijing_0_5";
char dir_geometry[200] = "../MCInfo/geometry_dpix30mum_wpix50mum.txt"; // directory of geometry.txt
char outputFilePrefix[200] = "digits/pi0_pt0-20_embedded";
char focalLibDir[200] = "../../FOCAL/FOCAL";

void digitizerEmbedding(Int_t startFolder = 1, Int_t endFolder = 1, Int_t startEvent = 0, Int_t endEvent = 99) {

//  Int_t startFolder = 1;
//  Int_t endFolder = 1;

//  TDatime tdm;
  
  TRandom3 * rnd = new TRandom3((endFolder*317 + startFolder*563 + 1));
  
  gROOT->LoadMacro(Form("%s/LoadFOCAL.C", focalLibDir));
  LoadFOCAL(focalLibDir);
  LoadFOCALSim(focalLibDir);
  
  AliFOCALDigitizer * digitizer = new AliFOCALDigitizer();
  AliFOCALGeometry * geom = AliFOCALGeometry::GetInstance(dir_geometry);
  digitizer->SetGeometry(geom);
  digitizer->Initialize("HS");
  
  // Variables for Signal loading:
  AliRunLoader *fRunLoaderSignal = 0;
  AliLoader *fFOCALLoaderSignal = 0;
  TTree * hitsTreeSignal = 0;
  
  // Variables for Background loading:
  TFile * hitsFileBackground = 0;
  TTree * hitsTreeBackground = 0;
  
  for(Int_t nfolder = startFolder; nfolder <= endFolder; nfolder++) {
    
    cout << "FOLDER: " << nfolder << endl;

    char filename[200];
    sprintf(filename,"%s/%i/%s",signalFolder,nfolder,"galice.root");
            
    // Create new output file per folder
    TFile * outputFile = new TFile(Form("%s_%i.root",outputFilePrefix,nfolder),"RECREATE");
           
//    char createSaveDir[100];
//    sprintf(createSaveDir,"mkdir -p images/rap_%.1f/%02i",rapidity,nfolder);
//    gSystem->Exec(createSaveDir);

            
    // Loop over events in the folder
    for (Int_t ievt = startEvent; ievt <= endEvent; ievt++) {
    
      // Chose random background event
	      
      Int_t bkgEvent = -1;
      Int_t bkgFolderNum = -1;
      if (randomizeBackground) {
        Int_t randomTotalEvent = (Int_t)(rnd->Integer(backgroundFolders*backgroundEventsPerFolder));
        bkgEvent = randomTotalEvent % backgroundEventsPerFolder;
        bkgFolderNum = (randomTotalEvent / backgroundEventsPerFolder) + 1;
      }
      else {
        Int_t totEventNum = nfolder*(endEvent+1) + ievt;
        bkgFolderNum = Int_t(totEventNum/backgroundEventsPerFolder) % backgroundFolders + 1;
        bkgEvent = totEventNum % backgroundEventsPerFolder;
      }

      // create directory in output file
      outputFile->mkdir(Form("Event%i",ievt));
      outputFile->cd(Form("Event%i",ievt));
      
      
      TObjString * sPar = new TObjString(Form("SimFolder=%s/%i",signalFolder,nfolder));
      sPar->Write();
      cout << sPar->String() << ", Event=" << ievt << endl;

      TObjString * sPar = new TObjString(Form("SignalFolder=%s/%i",signalFolder,nfolder));
      sPar->Write();
      cout << sPar->String() << ", Event=" << ievt << endl;
      
      sPar = new TObjString(Form("BackgroundFolder=%s/%i",backgroundFolder,bkgFolderNum));
      sPar->Write();
      cout << sPar->String() << ", Event=" << bkgEvent << endl;
      
      TParameter<Int_t> * iPar = new TParameter<Int_t>("SignalEvent",ievt);
      iPar->Write();
      
      iPar = new TParameter<Int_t>("BackgroundEvent",bkgEvent);
      iPar->Write();
      
      // Create output TTree
      TTree * digitTree = new TTree("DigitTree","FoCal digits tree");
      
      TClonesArray * digits; // = new TClonesArray("AliFOCALdigit",1000);
      Int_t bufsize = 32000;

      LoadSignalTree(hitsTreeSignal, filename, ievt, fRunLoaderSignal, fFOCALLoaderSignal);
      //LoadBackgroundTree(hitsTreeBackground, backgroundFolder, nfolder, ievt, hitsFileBackground);
      if (!LoadBackgroundTree(hitsTreeBackground, backgroundFolder, bkgFolderNum, bkgEvent, hitsFileBackground)) {
        bkgFolderNum--;
        if (bkgFolderNum < 0)
          bkgFolderNum = Int_t(gRandom->Rndm()*backgroundFolders) + 1;
        cout << "Bkg event not found; trying folder " << bkgFolderNum << " evt " << bkgEvent << endl;
        if (!LoadBackgroundTree(hitsTreeBackground, backgroundFolder, bkgFolderNum, bkgEvent, hitsFileBackground)) { // Could turn this into a while loop
          cout << "Skip event " << ievt << endl;
          continue;
        }
      }     
      cout << "\tEvent: " << ievt << " with " << hitsTreeSignal->GetEntries() << " tracks" << endl;
      cout << " bkg tree points " << hitsTreeBackground << endl;

      digitizer->Hits2DigitsEmbedding(hitsTreeSignal->GetBranch("FOCAL"),hitsTreeBackground->GetBranch("FOCAL"));
      digits = digitizer->GetSDigits();
      digitTree->Branch("Digits", &digits, bufsize);
      digitTree->Fill();
      outputFile->cd(Form("Event%i",ievt));
      digitTree->Write();
      digits->Delete();
      
      cout << "Number of digits: " << digits->GetEntries() << endl;
      
      
    }
    
    // Save output file!
    outputFile->Write();
    outputFile->Close();
    
  }
  
  delete rnd;
}

Bool_t LoadSignalTree(TTree & * tree, char * fileName, Int_t event, TObject & * oRunLoader, TObject & * oFocalLoader) {
  
  AliRunLoader * runLoader = (AliRunLoader*)oRunLoader;
  AliLoader * focalLoader = (AliLoader*)oFocalLoader;
  
  if (runLoader) {
    fRunLoader->Delete();
    runLoader = 0;
  }
           
  //check galice exists
  Long_t * id, *size, *flags, *mt;
  if (gSystem->GetPathInfo(fileName,id,size,flags,mt) == 1) {
      cout << "ERROR: "<< fileName << endl;
      return false;        
  }

  //Alice run loader
  runLoader = AliRunLoader::Open(fileName);
  
  if (!runLoader) {
      cout << "ERROR: "<< fileName << endl;
      return false;   
  }
  
  if (!runLoader->GetAliRun()) runLoader->LoadgAlice();
  if (!runLoader->TreeE()) runLoader->LoadHeader();
  if (!runLoader->TreeK()) runLoader->LoadKinematics();

  runLoader->GetAliRun()->GetDetector("FOCAL");
  focalLoader = runLoader->GetLoader("FOCALLoader");
  focalLoader->LoadHits("READ");
  
  runLoader->GetEvent(event);
  tree = focalLoader->TreeH();
  
  return true;
}

Bool_t LoadBackgroundTree(TTree & * tree, char * dirName, Int_t folder, Int_t event, TFile & * hitsFile) {
  
  if (hitsFile) {
    hitsFile->Close();
    hitsFile = 0;
  }
  
  hitsFile = new TFile(Form("%s/%i/FOCAL.Hits.root",dirName,folder),"READ");
  if (!hitsFile->cd(Form("Event%i",event)))
   return false;
  gDirectory->GetObject("TreeH",tree);
  if (tree == 0)
   return false;
  
  return true;
}
