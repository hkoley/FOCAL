#ifdef __CLING__
R__ADD_INCLUDE_PATH($FLUKA_VMC_ROOT/include/TFluka)
R__ADD_INCLUDE_PATH(/home/loizides/sw/FOCAL/FOCAL)
#endif

void loadg4libs()
{
/// Macro function for loading Geant4 libraries
/// from list of libraries build by geant4-config --libs 

  FILE* pipe = gSystem->OpenPipe("geant4-config --libs", "r");
  std::string all_lines;
  char line[1000];
  while ( fgets(line, sizeof(line), pipe ) != NULL ) {
    all_lines += line;
  }
  
  TString all_lines_t(all_lines.data());
  all_lines_t.Remove(all_lines_t.First('\n'));
  //cout << all_lines_t.Data() << endl;
  TObjArray* libs = all_lines_t.Tokenize(" ");

  //TString dynamicPath = gSystem->GetDynamicPath();
  for (Int_t i=libs->GetEntriesFast()-1; i>=0; i-- ) {
    TString addPath = ((TObjString*)libs->At(i))->GetString();
    if (addPath.BeginsWith("-L")) {
      addPath.Remove(0,2);
      addPath.ReplaceAll("\"", "");
      //cout << "Adding dynamic path " << addPath.Data() << endl;
      gSystem->AddDynamicPath(addPath.Data());
    }
  }

  //cout << libs->GetEntriesFast() << endl;
  for (Int_t i=libs->GetEntriesFast()-1; i>=0; i-- ) {
    TString lib = ((TObjString*)libs->At(i))->GetString();
    lib.ReplaceAll("-l", "lib");
    //cout << "Loading |" << lib.Data() << "|" << endl; 
    if(lib.BeginsWith("lib"))
      gSystem->Load(lib.Data());
  } 
  
  gSystem->SetFPEMask(0); 
  gSystem->Load("libg4root");
  gSystem->Load("libgeant4vmc");
}   

void loadfluka()
{
  gSystem->SetIncludePath("-I$FLUKA_VMC_ROOT/include/TFluka");
  //gSystem->Load("libfluka");    
  gSystem->Load("libflukavmc");    	
  cout << "\t* Instantiating TFluka..." << endl;
}

void Sim(){
  gSystem->SetIncludePath("-I$ROOTSYS/include -I$ALICE_ROOT/include\
                   -I$ALICE_ROOT -I$ALICE/geant3/TGeant3");
 
  gSystem->Load("liblhapdf");
  gSystem->Load("libEGPythia6");
  gSystem->Load("libpythia6");
  gSystem->Load("libAliPythia6");
  gSystem->Load("libHIJING");
  gSystem->Load("libTHijing");

  //gSystem->Load("libgeant321");
  //loadg4libs();
  loadfluka();

  //Load FOCAL libraries
  const Char_t *libdir = gSystem->ExpandPathName("$HOME/sw/FOCAL/FOCAL");
  //gROOT->LoadMacro(Form("%s/LoadFOCAL.C",libdir));
  //LoadFOCALSim(libdir);
  gROOT->ProcessLine(Form(".x %s/LoadFOCALSim.C(\"%s\")",libdir,libdir));

  AliSimulation sim;
  //sim.SetRunNumber(100);  // Setting the run number cause problems with GRP???
  //sim.SetSeed(__RANDOMSEED__);
  sim.SetSeed(clock()+gSystem->GetPid());
  sim.SetLoadAlignFromCDB(kFALSE);
  sim.SetUseDetectorsFromGRP(kFALSE);
  sim.SetDefaultStorage("local://$ALIROOT_OCDB_ROOT/OCDB");
  //sim.SetSpecificStorage("GRP/GRP/Data",Form("local://%s",gSystem->pwd()));

  sim.SetMakeDigits("");
  sim.SetMakeSDigits("");
  sim.SetMakeDigitsFromHits("");
  //sim.SetMakeSDigits("TRD TOF PHOS HMPID EMCAL MUON FMD ZDC PMD T0 VZERO");

  //sim.SetNumberOfEvents(1);
  //sim.SetRunGeneration(kTRUE);
  //sim.SetRunSimulation(kFALSE);
  //sim.Run();
  
  TStopwatch timer;
  timer.Start();
  sim.RunSimulation(100);
  timer.Stop();
  timer.Print();

}//Sim
